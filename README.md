# Image Tools (viktor-firus/image-tools)

This extension is a toolkit to generate responsive images in fluid templates (with optional lazy loading and opportunity to define animations yourself for lazy loading).

## TOC

- [Image reference](#image-reference)
- [ViewHelpers Reference](#viewhelpers-reference)
  - [ImgViewHelper](#imgviewhelper)
  - [PictureViewHelper](#pictureviewhelper)
  - [SourceViewHelper](#sourceviewhelper)
  - [LazyViewHelper](#lazyviewhelper)
  - [User defined animations](#user-defined-animations)
- [Demo](#demo)

## Image reference

For all view-helpers an "image-reference" is necessary, here are all options:

- `fileReference:123`: file reference from `sys_file_reference` with id `123`.
- `r:123`: abbreviation for `fileReference:123`.
- `file:234`: file from `sys_file` with id `234`.
- `f:234`: abbreviation for `file:234`.
- `{fileObject}`: instance of `\TYPO3\CMS\Extbase\Domain\Model\FileReference`
- `{fileObject}`: instance of `\TYPO3\CMS\Core\Resource\FileReference`
- `{fileObject}`: instance of `\TYPO3\CMS\Core\Resource\File`

or

- `{fileObject}`: instance of `\ByteCube\ImageTools\Image\ImageInterface`

## ViewHelpers Reference

In all examples the xml namespace `it` will used. The namespace depends on the view-helper import in your template.

### ImgViewHelper

#### Example

```xml
<it:img image="image-reference" width="1400" fallbackWidth="700" cropName="device1"/>
```

#### Arguments

| Argument  | Description | Type | Default |
| --- | --- | --- | --- |
| absolute | Force absolute image url. | bool | *false*  |
| alt | Alternate text. | string | *null* |
| aspectRatio | Aspect ratio for image using the format `16x9`. | string | *null* |
| cropName | Name of the crop variant from [image manipulation](https://docs.typo3.org/typo3cms/TCAReference/ColumnsConfig/Type/ImageManipulation.html#cropvariants). | string | *null* |
| fallbackWidth | If more than one `pixelDensity` is set a fallback image should be generated for the `src` attribute of the `img` tag. | int | *null* |
| image | See [image reference](#image-reference) | mixed | |
| pixelDensities | Image variants for the `srcset` attribute of the img tag using the format `1,2,3`. If more than one pixel density is set, the `img` tag will have a `src` attribute with the width from `fallbackWidth` and a `srcset` attribute with images generated from `pixelDensities`. | string | 1 |
| respectFocusArea | Respect the focus area if configured for [image manipulation](https://docs.typo3.org/typo3cms/TCAReference/ColumnsConfig/Type/ImageManipulation.html#cropvariants). | bool | *true* |
| width | Max width for image if needed. Multiplied with pixel densities. | int | *null* |

Further arguments initialized in [`AbstractTagBasedViewHelper`](https://github.com/TYPO3/Fluid/blob/master/src/Core/ViewHelper/AbstractTagBasedViewHelper.php) in method `initializeArguments` and `registerUniversalTagAttributes`.

### PictureViewHelper

#### Example

```xml
<it:picture image="image-reference" cropName="device2">
    <it:source width="1400" pixelDensities="1" cropName="device1" media="(min-width: 1400px)"/>
    <it:source width="1200" pixelDensities="1" media="(min-width: 1200px)"/>
    <it:source width="1000" pixelDensities="1" cropName="device3"/>
    <it:img width="700"/>
</it:picture>
```

#### Arguments

| Argument  | Description | Type | Default |
| --- | --- | --- | --- |
| absolute | Force absolute image url. | bool | *false*  |
| aspectRatio | Aspect ratio for image using the format `16x9`. | string | *null* |
| cropName | Name of the crop variant from [image manipulation](https://docs.typo3.org/typo3cms/TCAReference/ColumnsConfig/Type/ImageManipulation.html#cropvariants). | string | *null* |
| image | See [image reference](#image-reference) | mixed | |
| pixelDensities | Image variants for the child view-helpers using the format `1,2,3`. | string | 1 |
| respectFocusArea | Respect the focus area if configured for [image manipulation](https://docs.typo3.org/typo3cms/TCAReference/ColumnsConfig/Type/ImageManipulation.html#cropvariants). | bool | *true* |

`aspectRatio`, `cropName`, `pixelDensities` or `respectFocusArea` will apply to child `source` or `img` view-helpers if the attributes not defined in the child view-helpers.

Further arguments initialized in [`AbstractTagBasedViewHelper`](https://github.com/TYPO3/Fluid/blob/master/src/Core/ViewHelper/AbstractTagBasedViewHelper.php) in method `initializeArguments` and `registerUniversalTagAttributes`.

### SourceViewHelper

#### Example

```xml
<it:picture image="image-reference" cropName="device2">
    ...
    <it:source width="1200" pixelDensities="1" media="(min-width: 1200px)"/>
    ...
</it:picture>
```

#### Arguments

| Argument  | Description | Type | Default |
| --- | --- | --- | --- |
| aspectRatio | Aspect ratio for image using the format `16x9`. | string | *null* |
| cropName | Name of the crop variant from [image manipulation](https://docs.typo3.org/typo3cms/TCAReference/ColumnsConfig/Type/ImageManipulation.html#cropvariants). | string | *null* |
| image | See [image reference](#image-reference) | mixed | |
| pixelDensities | Image variants using the format `1,2,3`. | string | 1 |
| respectFocusArea | Respect the focus area if configured for [image manipulation](https://docs.typo3.org/typo3cms/TCAReference/ColumnsConfig/Type/ImageManipulation.html#cropvariants). | bool | *true* |
| width | Max width for image if needed. Multiplied with pixel densities. | int | *null* |

Further arguments initialized in [`AbstractTagBasedViewHelper`](https://github.com/TYPO3/Fluid/blob/master/src/Core/ViewHelper/AbstractTagBasedViewHelper.php) in method `initializeArguments` and `registerUniversalTagAttributes`.

### LazyViewHelper

Just wrap the `img` or the `picture` view-helper with the lazy view-helper.

#### Examples

```xml
<it:lazy placeholder="{placeholder}" animation="{animation}">
    <it:img image="image-reference" width="1400" fallbackWidth="700" cropName="device1"/>
</it:lazy>
```

```xml
<it:lazy placeholder="{placeholder}" animation="{animation}">
    <it:picture image="image-reference" cropName="device2">
        ...
    </it:picture>
</it:lazy>
```

#### Arguments

| Argument  | Description | Type | Default |
| --- | --- | --- | --- |
| placeholder | The initial loaded image. Can be a color-placeholder using the format `#FFF`/`#FFFFFF` or a blurred image using `blurred`. | string | #FFFFFF |
| animation | Animation for the lazy-loading. Current available animations `overlay` or `top-slide-overlay`. Leave it empty for no animation. | string | *null* |

Further arguments initialized in [`AbstractTagBasedViewHelper`](https://github.com/TYPO3/Fluid/blob/master/src/Core/ViewHelper/AbstractTagBasedViewHelper.php) in method `initializeArguments` and `registerUniversalTagAttributes`.

## User defined animations

There are 3 steps to define an animation for lazy-loading only using css.

The overlay dom-object is the picture which are visible before the overlay process starts.

The original dom-object is the picture which are visible after the overlay process ends.

### Step 1

The initial state of the overlay and original dom-object.

Default css rule of `placeholder` for this step has: `opacity: 1`

Default css rule of `original` for this step has: `opacity: 0`

### Step 2

Some additional css attributes for the initial state for the dom-objects.

**Please note!** For full compatibility with most browsers css-transitions should defined in step 2.

After step 2 is set, image-tools detect the highest delay of all defined transitions to know when step 3 ends.

### Step 3

This step is the end state if the dom-objects overlay and original.

Default css rule of `placeholder` for this step has: `opacity: 0`

Default css rule of `original` for this step has: `opacity: 1`

### Examples

#### Example 1

```css
div.imt-ll[data-imt-ll-animation="overlay"] img.placeholder.step-2 {
  transition: opacity 400ms ease-in;
}

div.imt-ll[data-imt-ll-animation="overlay"] img.original.step-2 {
  transition: opacity 400ms ease-out;
}
```

#### Example 2

```css
div.imt-ll[data-imt-ll-animation="top-slide-overlay"] img.placeholder.step-2 {
  transition: opacity 400ms linear;
}

div.imt-ll[data-imt-ll-animation="top-slide-overlay"] img.original.step-1 {
  top: 40px;
}
div.imt-ll[data-imt-ll-animation="top-slide-overlay"] img.original.step-2 {
  transition: top 400ms linear, opacity 400ms linear;
}
div.imt-ll[data-imt-ll-animation="top-slide-overlay"] img.original.step-3 {
  top: 0;
}
```

## Demo

To see a demo of the functionality enable the demo backend module.

TYPO3 V9: "Admin Tools" > "Settings" > "Extension Configuration" > "image_tools" > Activate "Enable demo module."
