<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Tests\Unit;

use Mockery as m;
use Mockery\Mock;
use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\TestingFramework\Core\BaseTestCase;
use ViktorFirus\ImageTools\Storage;
use ViktorFirus\ImageTools\Utility;

class StorageTest extends BaseTestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var \ViktorFirus\ImageTools\Tests\Unit\Mock
     */
    private $mock;

    protected function setUp()
    {
        parent::setUp();

        $this->mock = new \ViktorFirus\ImageTools\Tests\Unit\Mock();

        $this->storage = new Storage($this->mock->resourceStorage());
        $this->storage->injectObjectManager($this->mock->objectManager());
        $this->storage->injectImageService($this->mock->imageService());
    }

    public function testHasImage()
    {
        $resourceStorage = $this->mock->resourceStorage();
        $resourceStorage
            ->shouldReceive('hasFile')->with('image-identifier')->andReturnTrue();

        $this->assertTrue($this->storage->hasImage('image-identifier'));
    }

    public function testGetImage()
    {
        $resourceStorage = $this->mock->resourceStorage();
        $resourceStorage
            ->shouldReceive('getFile')->with('image-identifier')->andReturn($this->mock->file());

        $this->storage->getImage('image-identifier');
    }

    public function testSaveImage()
    {
        $utility = m::mock(Utility::class);
        $utility
            ->shouldReceive('dirname')->with('image/subfolder/identifier.jpg')->andReturn('image/subfolder')->getMock()
            ->shouldReceive('basename')->with('image/subfolder/identifier.jpg')->andReturn('identifier.jpg');

        $this->storage->injectUtility($utility);

        $folder = m::mock(Folder::class);
        $folder
            ->shouldReceive('hasFile')->with('identifier.jpg')->andReturnTrue();

        $resourceStorage = $this->mock->resourceStorage();
        $resourceStorage
            ->shouldReceive('hasFolder')->with('image/subfolder')->andReturnTrue()->getMock()
            ->shouldReceive('getFolder')->with('image/subfolder')->andReturn($folder)->getMock()
            ->shouldReceive('getFile')->with('image/subfolder/identifier.jpg')->andReturn($this->mock->file());

        $file = $this->mock->file();
        $file
            ->shouldReceive('setContents')->with('image-content');

        $this->storage->saveImage('image/subfolder/identifier.jpg', 'image-content');
    }

    public function testAddImage()
    {
        $folder = m::mock(Folder::class);
        $folder
            ->shouldReceive('addFile')->with('/source/image/file.jpg', null, DuplicationBehavior::REPLACE)->andReturn($this->mock->file());

        $resourceStorage = $this->mock->resourceStorage();
        $resourceStorage
            ->shouldReceive('hasFolder')->with('image/subfolder')->andReturnTrue()->getMock()
            ->shouldReceive('getFolder')->with('image/subfolder')->andReturn($folder);

        $this->storage->addImage('/source/image/file.jpg', 'image/subfolder');
    }
}
