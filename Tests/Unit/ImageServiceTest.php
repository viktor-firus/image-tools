<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Tests\Unit;

use ByteCube\ImageTools\Crop\CropDimensions;
use ByteCube\ImageTools\Image\ImageInterface;
use Mockery as m;
use Mockery\Mock;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Utility\CommandUtility;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;
use TYPO3\TestingFramework\Core\BaseTestCase;
use ViktorFirus\ImageTools\ImageInfo;
use ViktorFirus\ImageTools\ImageService;
use ViktorFirus\ImageTools\Utility;

define('PATH_site', '/site/path/');

class ImageServiceTest extends BaseTestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @var ImageService
     */
    protected $imageService;

    /**
     * @var Utility|Mock
     */
    protected $utility;

    /**
     * @var CommandUtility|Mock
     */
    protected $commandUtility;

    /**
     * @var \TYPO3\CMS\Extbase\Service\ImageService|Mock
     */
    protected $extbaseImageService;

    /**
     * @var ObjectManagerInterface|Mock
     */
    protected $objectManager;

    /**
     * @var \ViktorFirus\ImageTools\Tests\Unit\Mock
     */
    protected $mock;

    protected function setUp()
    {
        parent::setUp();

        $this->mock = new \ViktorFirus\ImageTools\Tests\Unit\Mock();

        $this->utility = m::mock(Utility::class);
        $this->commandUtility = m::mock(CommandUtility::class);
        $this->extbaseImageService = m::mock(\TYPO3\CMS\Extbase\Service\ImageService::class);
        $this->objectManager = $this->mock->objectManager();

        $this->imageService = new ImageService();
        $this->imageService->injectUtility($this->utility);
        $this->imageService->injectCommandUtility($this->commandUtility);
        $this->imageService->injectImageService($this->extbaseImageService);
        $this->imageService->injectObjectManager($this->objectManager);
    }

    public function testCrop()
    {
        $file = $this->mock->file();

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('getOriginal')->andReturn($file);

        $cropDimensions = new CropDimensions(10, 15, 20, 25);

        $processedFile = m::mock(ProcessedFile::class);
        $processedFile
            ->shouldReceive('getStorage')->andReturn($this->mock->resourceStorage());

        $imageServiceInstructions = ['width' => null, 'height' => null, 'minWidth' => null, 'minHeight' => null, 'maxWidth' => 150, 'maxHeight' => null, 'crop' => '{"x":10,"y":15,"width":20,"height":25,"rotate":0}'];
        $this->extbaseImageService
            ->shouldReceive('applyProcessingInstructions')->with($file, $imageServiceInstructions)->andReturn($processedFile);

        $this->objectManager
            ->shouldReceive('get')->with(ImageInfo::class, $processedFile)->andReturn($this->mock->imageInfo());

        $this->imageService->crop($image, 150, 2.0, $cropDimensions);
    }

    public function testBlurExisting()
    {
        $imageInfo = $this->mock->imageInfo();

        $blurredImage = m::mock(ImageInterface::class);

        $storage = $this->mock->storage();
        $storage
            ->shouldReceive('hasImage')->with('image-tools/placeholder/blurred/e43f32f6c019b9d57317242dbb353aa6bd8231df.jpg')->andReturnTrue()->getMock()
            ->shouldReceive('getImage')->with('image-tools/placeholder/blurred/e43f32f6c019b9d57317242dbb353aa6bd8231df.jpg')->andReturn($blurredImage);

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('getImageInfo')->andReturn($imageInfo)->getMock()
            ->shouldReceive('getStorage')->andReturn($storage);

        $this->assertSame($blurredImage, $this->imageService->blur($image));
    }

    public function testBlur()
    {
        $imageInfo = $this->mock->imageInfo();

        $blurredImage = m::mock(ImageInterface::class);

        $storage = $this->mock->storage();
        $storage
            ->shouldReceive('hasImage')->with('image-tools/placeholder/blurred/e43f32f6c019b9d57317242dbb353aa6bd8231df.jpg')->andReturnFalse()->getMock()
            ->shouldReceive('addImage')->with('/site/path/typo3temp/ImageTools/e43f32f6c019b9d57317242dbb353aa6bd8231df.jpg', 'image-tools/placeholder/blurred')->andReturn($blurredImage);

        $image = m::mock(ImageInterface::class);
        $image
            ->shouldReceive('getImageInfo')->andReturn($imageInfo)->getMock()
            ->shouldReceive('getStorage')->andReturn($storage);

        $this->utility->shouldReceive('mkdirDeep');

        $imageMagickCommand = '-resize "25%" -gaussian-blur "0x8" -quality 60 "/path/to/image" "/site/path/typo3temp/ImageTools/e43f32f6c019b9d57317242dbb353aa6bd8231df.jpg"';

        $this->commandUtility
            ->shouldReceive('escapeShellArgument')->with('25%')->andReturn('"25%"')->getMock()
            ->shouldReceive('escapeShellArgument')->with('0x8')->andReturn('"0x8"')->getMock()
            ->shouldReceive('escapeShellArgument')->with('/path/to/image')->andReturn('"/path/to/image"')->getMock()
            ->shouldReceive('escapeShellArgument')->with('/site/path/typo3temp/ImageTools/e43f32f6c019b9d57317242dbb353aa6bd8231df.jpg')->andReturn('"/site/path/typo3temp/ImageTools/e43f32f6c019b9d57317242dbb353aa6bd8231df.jpg"')->getMock()
            ->shouldReceive('imageMagickCommand')->with('convert', $imageMagickCommand)->andReturn('image-magick-command')->getMock()
            ->shouldReceive('exec')->with('image-magick-command');

        $this->assertSame($blurredImage, $this->imageService->blur($image));
    }
}
