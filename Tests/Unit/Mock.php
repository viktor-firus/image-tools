<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Tests\Unit;

use ByteCube\ImageTools\StorageInterface;
use Mockery as m;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;
use ViktorFirus\ImageTools\ImageInfo;
use ViktorFirus\ImageTools\ImageService;
use ViktorFirus\ImageTools\Storage;

class Mock
{
    /**
     * @var ImageInfo|m\Mock
     */
    private $imageInfo;

    /**
     * @var ResourceStorage|m\Mock
     */
    private $resourceStorage;

    /**
     * @var FileInterface|m\Mock
     */
    private $file;

    /**
     * @var ObjectManagerInterface|m\Mock
     */
    private $objectManager;

    /**
     * @var StorageInterface|m\Mock
     */
    private $storage;

    /**
     * @var ImageService|m\Mock
     */
    private $imageService;

    /**
     * @return ImageInfo|m\Mock
     */
    public function imageInfo(): ImageInfo
    {
        if ($this->imageInfo) {
            return $this->imageInfo;
        }

        $this->imageInfo = m::mock(ImageInfo::class);
        $this->imageInfo
            ->shouldReceive('getWidth')->andReturn(400)->getMock()
            ->shouldReceive('getHeight')->andReturn(300)->getMock()
            ->shouldReceive('getHashIdentifier')->andReturn('e43f32f6c019b9d57317242dbb353aa6bd8231df')->getMock()
            ->shouldReceive('getPath')->andReturn('/path/to/image');

        return $this->imageInfo;
    }

    /**
     * @return ResourceStorage|m\Mock
     */
    public function resourceStorage(): ResourceStorage
    {
        if ($this->resourceStorage) {
            return $this->resourceStorage;
        }

        $this->resourceStorage = m::mock(ResourceStorage::class);

        return $this->resourceStorage;
    }

    /**
     * @return FileInterface|m\Mock
     */
    public function file(): FileInterface
    {
        if ($this->file) {
            return $this->file;
        }

        $this->file = m::mock(FileInterface::class);
        $this->file
            ->shouldReceive('getStorage')->andReturn(self::resourceStorage())->getMock()
            ->shouldReceive('getSha1')->andReturn('6fb023bd9342359d5a1697339aef2146237e29ac')->getMock()
            ->shouldReceive('getProperty')->with('width')->andReturn('400')->getMock()
            ->shouldReceive('getProperty')->with('height')->andReturn('300')->getMock()
            ->shouldReceive('getProperty')->with('alternative')->andReturn('image-alternative')->getMock()
            ->shouldReceive('getProperty')->with('title')->andReturn('image-title')->getMock()
            ->shouldReceive('getForLocalProcessing')->with(false)->andReturn('/file/path');

        return $this->file;
    }

    /**
     * @return ObjectManagerInterface|m\Mock
     */
    public function objectManager(): ObjectManagerInterface
    {
        if ($this->objectManager) {
            return $this->objectManager;
        }

        $this->objectManager = m::mock(ObjectManagerInterface::class);
        $this->objectManager
            ->shouldReceive('get')->with(ImageInfo::class, $this->file())->andReturn($this->imageInfo())->getMock()
            ->shouldReceive('get')->with(Storage::class, $this->resourceStorage())->andReturn($this->storage());

        return $this->objectManager;
    }

    /**
     * @return StorageInterface|m\Mock
     */
    public function storage(): StorageInterface
    {
        if ($this->storage) {
            return $this->storage;
        }

        $this->storage = m::mock(StorageInterface::class);

        return $this->storage;
    }

    /**
     * @return ImageService|m\Mock
     */
    public function imageService(): ImageService
    {
        if ($this->imageService) {
            return $this->imageService;
        }

        $this->imageService = m::mock(ImageService::class);

        return $this->imageService;
    }
}
