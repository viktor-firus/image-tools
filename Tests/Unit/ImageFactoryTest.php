<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Tests\Unit;

use ByteCube\ImageTools\Image\ImageInterface;
use InvalidArgumentException;
use Mockery as m;
use Mockery\Mock;
use RuntimeException;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;
use TYPO3\TestingFramework\Core\BaseTestCase;
use ViktorFirus\ImageTools\ImageFactory;
use ViktorFirus\ImageTools\ImageService;

class ImageFactoryTest extends BaseTestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @var ObjectManagerInterface|Mock
     */
    protected $objectManager;

    /**
     * @var FileRepository|Mock
     */
    protected $fileRepository;

    /**
     * @var ImageService|Mock
     */
    protected $imageService;

    /**
     * @var ImageFactory
     */
    protected $imageFactory;

    /**
     * @var \ViktorFirus\ImageTools\Tests\Unit\Mock
     */
    protected $mock;

    protected function setUp()
    {
        $this->mock = new \ViktorFirus\ImageTools\Tests\Unit\Mock();

        $this->fileRepository = m::mock(FileRepository::class);
        $this->objectManager = $this->mock->objectManager();
        $this->imageService = m::mock(ImageService::class);

        $this->imageFactory = new ImageFactory();
        $this->imageFactory->injectFileRepository($this->fileRepository);
        $this->imageFactory->injectObjectManager($this->objectManager);
        $this->imageFactory->injectImageService($this->imageService);
    }

    protected function setUpCreateFromFileReference(): FileReference
    {
        $crop = [
            'default' => [
                'cropArea' => ['x' => 0.1, 'y' => 0.15, 'width' => 0.2, 'height' => 0.25],
                'focusArea' => ['x' => 0.3, 'y' => 0.35, 'width' => 0.4, 'height' => 0.45],
            ],
        ];

        $file = $this->mock->file();

        $fileReference = m::mock(FileReference::class);
        $fileReference
            ->shouldReceive('getProperty')->with('crop')->andReturn(json_encode($crop))->getMock()
            ->shouldReceive('getOriginalFile')->andReturn($file);

        return $fileReference;
    }

    protected function setUpCreateFromFileReferenceId()
    {
        $fileReference = $this->setUpCreateFromFileReference();

        $this->fileRepository->shouldReceive('findFileReferenceByUid')->with(13)->andReturn($fileReference);
    }

    protected function setUpCreateFromFileId()
    {
        $file = $this->mock->file();

        $this->fileRepository->shouldReceive('findByUid')->with(13)->andReturn($file);
    }

    public function testCreateFromInstanceOfImageInterface()
    {
        $image = m::mock(ImageInterface::class);
        $this->assertSame($image, $this->imageFactory->create($image));
    }

    public function testCreateFromFileReferenceIdShortSyntax()
    {
        $this->setUpCreateFromFileReferenceId();
        $this->imageFactory->create('r:13');
    }

    public function testCreateFromFileReferenceIdLongSyntax()
    {
        $this->setUpCreateFromFileReferenceId();
        $this->imageFactory->create('fileReference:13');
    }

    public function testCreateFromFileIdShortSyntax()
    {
        $this->setUpCreateFromFileId();
        $this->imageFactory->create('f:13');
    }

    public function testCreateFromFileIdLongSyntax()
    {
        $this->setUpCreateFromFileId();
        $this->imageFactory->create('file:13');
    }

    public function testCreateFromInvalidFileId()
    {
        $this->fileRepository->shouldReceive('findByUid')->andThrow(RuntimeException::class, '', 1314354065);
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionCode(1535726505);

        $this->imageFactory->create('file:13');
    }

    public function testCreateFromFileIdWithUnknownRuntimeException()
    {
        $this->fileRepository->shouldReceive('findByUid')->andThrow(RuntimeException::class);
        $this->expectException(RuntimeException::class);

        $this->imageFactory->create('file:13');
    }

    public function testCreateFromInstanceOfFileReference()
    {
        $fileReference = $this->setUpCreateFromFileReference();

        $this->imageFactory->create($fileReference);
    }

    public function testCreateFromInstanceOfFileReferenceDomainObject()
    {
        $fileReference = $this->setUpCreateFromFileReference();

        $fileReferenceDomainModel = m::mock(\TYPO3\CMS\Extbase\Domain\Model\FileReference::class);
        $fileReferenceDomainModel->shouldReceive('getOriginalResource')->withNoArgs()->andReturn($fileReference);

        $this->imageFactory->create($fileReferenceDomainModel);
    }

    public function testCreateFromInvalidCreateArgument()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionCode(1535726505);
        $this->imageFactory->create('blalba');
    }
}