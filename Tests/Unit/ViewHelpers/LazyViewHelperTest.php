<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Tests\Unit\ViewHelpers;

use Exception;
use Mockery as m;
use Mockery\Mock;
use TYPO3\TestingFramework\Core\BaseTestCase;
use TYPO3Fluid\Fluid\Core\Variables\VariableProviderInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;
use ViktorFirus\ImageTools\ViewHelpers\LazyViewHelper;

class LazyViewHelperTest extends BaseTestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @var LazyViewHelper|Mock
     */
    protected $viewHelper;

    /**
     * @var VariableProviderInterface|Mock
     */
    protected $templateVariableContainer;

    /**
     * @var TagBuilder|Mock
     */
    protected $tag;

    protected function setUp()
    {
        parent::setUp();

        $this->viewHelper = m::mock(LazyViewHelper::class)->makePartial();

        $this->tag = m::mock(TagBuilder::class);
        $this->tag->shouldReceive('reset')->withNoArgs()->getMock()
            ->shouldReceive('setTagName')->with('div');
        $this->inject($this->viewHelper, 'tag', $this->tag);

        $this->templateVariableContainer = m::mock(VariableProviderInterface::class);
        $this->inject($this->viewHelper, 'templateVariableContainer', $this->templateVariableContainer);
    }

    public function testRender()
    {
        $this->viewHelper
            ->shouldReceive('renderChildren')->andReturn('-children-content-');

        $this->templateVariableContainer
            ->shouldReceive('exists')->with('image_tools_lazy_loading_placeholder')->andReturnFalse()->getMock()
            ->shouldReceive('add')->getMock()
            ->shouldReceive('remove')->with('image_tools_lazy_loading_placeholder')->getMock();

        $this->tag
            ->shouldReceive('addAttribute')->with('data-imt-ll', '')->getMock()
            ->shouldReceive('getAttribute')->with('class')->andReturn('some-other-class')->getMock()
            ->shouldReceive('addAttribute')->with('class', 'some-other-class imt-ll')->getMock()
            ->shouldReceive('addAttribute')->with('data-imt-ll-animation', '-animation-name-')->getMock()
            ->shouldReceive('setContent')->with('-children-content-')->getMock()
            ->shouldReceive('render')->andReturn('');

        $this->viewHelper->setArguments([
            'placeholder' => '#123',
            'animation' => '-animation-name-',
        ]);
        $this->viewHelper->initializeArgumentsAndRender();
    }

    public function testRenderInsideOfLazyTag()
    {
        $this->templateVariableContainer
            ->shouldReceive('exists')->with('image_tools_lazy_loading_placeholder')->andReturnTrue()->getMock();

        $this->expectException(Exception::class);
        $this->expectExceptionCode(1538290450);

        $this->viewHelper->initializeArgumentsAndRender();
    }
}
