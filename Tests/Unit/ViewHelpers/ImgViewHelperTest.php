<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Tests\Unit\ViewHelpers;

use ByteCube\ImageTools\Element\ImgElement;
use ByteCube\ImageTools\ElementBuilder\ImgElementBuilder;
use ByteCube\ImageTools\ElementBuilder\PictureElementBuilder;
use ByteCube\ImageTools\Placeholder;
use Mockery as m;
use Mockery\Mock;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\TestingFramework\Core\BaseTestCase;
use TYPO3Fluid\Fluid\Core\Variables\VariableProviderInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;
use ViktorFirus\ImageTools\ViewHelpers\ImgViewHelper;

class ImgViewHelperTest extends BaseTestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @var ImgViewHelper|Mock
     */
    protected $viewHelper;

    /**
     * @var VariableProviderInterface|Mock
     */
    protected $templateVariableContainer;

    /**
     * @var ImgElement|Mock
     */
    protected $imgElement;

    /**
     * @var ImgElementBuilder|Mock
     */
    protected $imgElementBuilder;

    /**
     * @var ObjectManager|Mock
     */
    protected $objectManager;

    protected function setUp()
    {
        parent::setUp();

        $this->viewHelper = m::mock(ImgViewHelper::class)->makePartial();

        $tag = m::mock(TagBuilder::class);
        $tag->shouldReceive('reset')->withNoArgs()->getMock()
            ->shouldReceive('setTagName')->with('img');
        $this->inject($this->viewHelper, 'tag', $tag);

        $this->templateVariableContainer = m::mock(VariableProviderInterface::class);
        $this->inject($this->viewHelper, 'templateVariableContainer', $this->templateVariableContainer);

        $this->imgElement = m::mock(ImgElement::class);

        $this->imgElementBuilder = m::mock(ImgElementBuilder::class);
        $this->imgElementBuilder
            ->shouldReceive('setWidth')->with(123)->getMock()
            ->shouldReceive('setTag')->with($tag)->andReturnSelf();

        $this->objectManager = m::mock(ObjectManager::class);
        $this->viewHelper->injectObjectManager($this->objectManager);
    }

    public function testRender()
    {
        $this->objectManager->shouldReceive('get')->with(ImgElementBuilder::class)->andReturn($this->imgElementBuilder);

        $this->templateVariableContainer
            ->shouldReceive('exists')->with('image_tools_picture_element_builder')->andReturnFalse()->getMock()
            ->shouldReceive('exists')->with('image_tools_lazy_loading_placeholder')->andReturnFalse();
        $this->imgElementBuilder
            ->shouldReceive('setFallbackWidth')->with(22)->getMock()
            ->shouldReceive('setRespectFocusArea')->with(false)->andReturnSelf()->getMock()
            ->shouldReceive('build')->with('imgRes')->andReturn($this->imgElement);
        $this->imgElement->shouldReceive('renderRelative')->withNoArgs();

        $this->viewHelper->setArguments([
            'image' => 'imgRes',
            'width' => 123,
            'fallbackWidth' => 22,
            'absolute' => false,
            'respectFocusArea' => false,
            'cropName' => null,
            'aspectRatio' => null,
            'pixelDensities' => null,
        ]);
        $this->viewHelper->initializeArgumentsAndRender();
    }

    public function testRenderWithPlaceholder()
    {
        $placeholder = m::mock(Placeholder::class);

        $this->objectManager->shouldReceive('get')->with(ImgElementBuilder::class)->andReturn($this->imgElementBuilder);

        $this->templateVariableContainer
            ->shouldReceive('exists')->with('image_tools_picture_element_builder')->andReturnFalse()->getMock()
            ->shouldReceive('exists')->with('image_tools_lazy_loading_placeholder')->andReturnTrue()->getMock()
            ->shouldReceive('get')->with('image_tools_lazy_loading_placeholder')->andReturn($placeholder);
        $this->imgElementBuilder
            ->shouldReceive('setFallbackWidth')->with(22)->getMock()
            ->shouldReceive('setRespectFocusArea')->with(false)->andReturnSelf()->getMock()
            ->shouldReceive('build')->with('imgRes')->andReturn($this->imgElement)->getMock()
            ->shouldReceive('setPlaceholder')->with($placeholder);
        $this->imgElement->shouldReceive('renderRelative')->withNoArgs();

        $this->viewHelper->setArguments([
            'image' => 'imgRes',
            'width' => 123,
            'fallbackWidth' => 22,
            'absolute' => false,
            'respectFocusArea' => false,
            'cropName' => null,
            'aspectRatio' => null,
            'pixelDensities' => null,
        ]);
        $this->viewHelper->initializeArgumentsAndRender();
    }

    public function testRenderWithAbsolutePath()
    {
        $this->objectManager->shouldReceive('get')->with(ImgElementBuilder::class)->andReturn($this->imgElementBuilder);

        $this->templateVariableContainer
            ->shouldReceive('exists')->with('image_tools_picture_element_builder')->andReturnFalse()->getMock()
            ->shouldReceive('exists')->with('image_tools_lazy_loading_placeholder')->andReturnFalse();
        $this->imgElementBuilder
            ->shouldReceive('setFallbackWidth')->with(22)->getMock()
            ->shouldReceive('setRespectFocusArea')->with(false)->andReturnSelf()->getMock()
            ->shouldReceive('build')->with('imgRes')->andReturn($this->imgElement);
        $this->imgElement->shouldReceive('renderAbsolute')->withNoArgs();

        $this->viewHelper->setArguments([
            'image' => 'imgRes',
            'width' => 123,
            'fallbackWidth' => 22,
            'absolute' => true,
            'respectFocusArea' => false,
            'cropName' => null,
            'aspectRatio' => null,
            'pixelDensities' => null,
        ]);
        $this->viewHelper->initializeArgumentsAndRender();
    }

    public function testRenderInsideOfPictureTag()
    {
        $pictureElementBuilder = m::mock(PictureElementBuilder::class);
        $pictureElementBuilder->shouldReceive('createImgElementBuilder')->andReturn($this->imgElementBuilder);

        $this->templateVariableContainer
            ->shouldReceive('exists')->with('image_tools_picture_element_builder')->andReturnTrue()->getMock()
            ->shouldReceive('get')->with('image_tools_picture_element_builder')->andReturn($pictureElementBuilder)->getMock()
            ->shouldReceive('exists')->with('image_tools_lazy_loading_placeholder')->andReturnFalse();

        $this->imgElementBuilder
            ->shouldReceive('setCropName')->with('bla')->andReturnSelf()->getMock()
            ->shouldReceive('setAspectRatio')->andReturnSelf()->getMock()
            ->shouldReceive('setPixelDensities')->with([1, 2, 3])->andReturnSelf()->getMock()
            ->shouldReceive('setRespectFocusArea')->with(false)->andReturnSelf()->getMock()
            ->shouldReceive('setFallbackWidth')->with(22)->getMock()
            ->shouldReceive('setImage')->with('imgRes')->andReturnSelf();

        $this->viewHelper->setArguments([
            'image' => 'imgRes',
            'width' => 123,
            'fallbackWidth' => 22,
            'cropName' => 'bla',
            'aspectRatio' => '12x2',
            'pixelDensities' => '1,2,3',
            'respectFocusArea' => false,
            'absolute' => false,
        ]);

        $this->viewHelper->initializeArgumentsAndRender();
    }
}
