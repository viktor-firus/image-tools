<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Tests\Unit\ViewHelpers;

use ByteCube\ImageTools\ElementBuilder\PictureElementBuilder;
use ByteCube\ImageTools\ElementBuilder\SourceElementBuilder;
use Mockery as m;
use Mockery\Mock;
use TYPO3\TestingFramework\Core\BaseTestCase;
use TYPO3Fluid\Fluid\Core\Variables\VariableProviderInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;
use ViktorFirus\ImageTools\ViewHelpers\SourceViewHelper;

class SourceViewHelperTest extends BaseTestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @var SourceViewHelper|Mock
     */
    protected $viewHelper;

    /**
     * @var VariableProviderInterface|Mock
     */
    protected $templateVariableContainer;

    /**
     * @var TagBuilder
     */
    protected $tag;

    public function setUp()
    {
        parent::setUp();

        $this->viewHelper = m::mock(SourceViewHelper::class)->makePartial();

        $this->viewHelper->setArguments([
            'respectFocusArea' => false,
            'cropName' => null,
            'aspectRatio' => null,
            'pixelDensities' => null,
        ]);

        $this->tag = m::mock(TagBuilder::class);
        $this->tag
            ->shouldReceive('reset')->withNoArgs()->once()->getMock()
            ->shouldReceive('setTagName')->with('source')->once();
        $this->inject($this->viewHelper, 'tag', $this->tag);

        $this->templateVariableContainer = m::mock(VariableProviderInterface::class);
        $this->inject($this->viewHelper, 'templateVariableContainer', $this->templateVariableContainer);
    }

    public function testRenderOutsideOfPictureTag()
    {
        $this->templateVariableContainer
            ->shouldReceive('exists')->with('image_tools_lazy_loading_placeholder')->andReturnFalse()->once()->getMock()
            ->shouldReceive('exists')->with('image_tools_picture_element_builder')->andReturnFalse()->once();
        $this->expectException(\Exception::class);
        $this->expectExceptionCode(1536408441);
        $this->viewHelper->initializeArgumentsAndRender();
    }

    public function testRender()
    {
        $this->viewHelper->setArguments([
            'image' => 'imgRes',
            'width' => 123,
            'respectFocusArea' => false,
            'cropName' => null,
            'aspectRatio' => null,
            'pixelDensities' => null,
        ]);

        $sourceElementBuilder = m::mock(SourceElementBuilder::class);
        $sourceElementBuilder
            ->shouldReceive('setRespectFocusArea')->with(false)->andReturnSelf()->once()->getMock()
            ->shouldReceive('setWidth')->with(123)->once()->getMock()
            ->shouldReceive('setTag')->with($this->tag)->andReturnSelf()->once()->getMock()
            ->shouldReceive('setImage')->with('imgRes')->andReturnSelf()->once();

        $pictureElementBuilder = m::mock(PictureElementBuilder::class);
        $pictureElementBuilder->shouldReceive('createSourceElementBuilder')->withNoArgs()->andReturn($sourceElementBuilder)->once();

        $this->templateVariableContainer
            ->shouldReceive('exists')->with('image_tools_lazy_loading_placeholder')->andReturnFalse()->once()->getMock()
            ->shouldReceive('exists')->with('image_tools_picture_element_builder')->andReturnTrue()->once()->getMock()
            ->shouldReceive('get')->with('image_tools_picture_element_builder')->andReturn($pictureElementBuilder)->once();

        $this->viewHelper->initializeArgumentsAndRender();
    }
}
