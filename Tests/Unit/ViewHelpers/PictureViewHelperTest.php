<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Tests\Unit\ViewHelpers;

use ByteCube\ImageTools\Element\PictureElement;
use ByteCube\ImageTools\ElementBuilder\PictureElementBuilder;
use ByteCube\ImageTools\Placeholder;
use Exception;
use Mockery as m;
use Mockery\Mock;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\TestingFramework\Core\BaseTestCase;
use TYPO3Fluid\Fluid\Core\Parser\SyntaxTree\ViewHelperNode;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\Variables\VariableProviderInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;
use ViktorFirus\ImageTools\ViewHelpers\PictureViewHelper;

class PictureViewHelperTest extends BaseTestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @var PictureViewHelper|Mock
     */
    protected $viewHelper;

    /**
     * @var VariableProviderInterface|Mock
     */
    protected $templateVariableContainer;

    /**
     * @var TagBuilder|Mock
     */
    protected $tag;

    /**
     * @var PictureElementBuilder|Mock
     */
    protected $pictureElementBuilder;

    /**
     * @var ObjectManager|Mock
     */
    protected $objectManager;

    protected function setUp()
    {
        parent::setUp();

        $this->viewHelper = m::mock(PictureViewHelper::class)->makePartial();

        $this->tag = m::mock(TagBuilder::class);
        $this->tag
            ->shouldReceive('reset')->withNoArgs()->once()->getMock()
            ->shouldReceive('setTagName')->with('picture')->once();
        $this->inject($this->viewHelper, 'tag', $this->tag);

        $this->templateVariableContainer = m::mock(VariableProviderInterface::class);
        $this->inject($this->viewHelper, 'templateVariableContainer', $this->templateVariableContainer);
    }

    protected function setUpRenderFirstPart(bool $placeholder = false)
    {
        $this->pictureElementBuilder = m::mock(PictureElementBuilder::class);

        $this->templateVariableContainer
            ->shouldReceive('exists')->with('image_tools_lazy_loading_placeholder')->andReturn($placeholder)->once()->getMock()
            ->shouldReceive('exists')->with('image_tools_picture_element_builder')->andReturnFalse()->once()->getMock()
            ->shouldReceive('add')->with('image_tools_picture_element_builder', $this->pictureElementBuilder)->once();

        $renderingContext = m::mock(RenderingContextInterface::class);
        $renderingContext
            ->shouldReceive('getVariableProvider')->withNoArgs()->andReturn($this->templateVariableContainer)->once()->getMock()
            ->shouldReceive('getViewHelperVariableContainer')->withNoArgs()->once();
        $this->inject($this->viewHelper, 'renderingContext', $renderingContext);

        $viewHelperNode = m::mock(ViewHelperNode::class);
        $viewHelperNode->shouldReceive('evaluateChildNodes')->withAnyArgs()->andReturn('')->once();
        $this->inject($this->viewHelper, 'viewHelperNode', $viewHelperNode);

        $this->objectManager = m::mock(ObjectManager::class);
        $this->objectManager->shouldReceive('get')->with(PictureElementBuilder::class)->andReturn($this->pictureElementBuilder)->once();
        $this->viewHelper->injectObjectManager($this->objectManager);
    }

    protected function setUpRender(bool $absolutePath = false, bool $placeholder = false)
    {
        $this->setUpRenderFirstPart($placeholder);

        $this->viewHelper->setArguments([
            'image' => 'imgRes',
            'absolute' => $absolutePath,
            'respectFocusArea' => false,
            'cropName' => null,
            'aspectRatio' => null,
            'pixelDensities' => null,
        ]);

        $this->templateVariableContainer->shouldReceive('remove')->with('image_tools_picture_element_builder')->once();

        $pictureElement = m::mock(PictureElement::class);
        $absolutePath
            ? $pictureElement->shouldReceive('renderAbsolute')->withNoArgs()->andReturn('<picture>')->once()
            : $pictureElement->shouldReceive('renderRelative')->withNoArgs()->andReturn('<picture>')->once();

        $this->pictureElementBuilder
            ->shouldReceive('setRespectFocusArea')->with(false)->andReturnSelf()->once()->getMock()
            ->shouldReceive('hasSourceElementBuilder')->withNoArgs()->andReturnTrue()->once()->getMock()
            ->shouldReceive('hasImgElementBuilder')->withNoArgs()->andReturnTrue()->once()->getMock()
            ->shouldReceive('setTag')->with($this->tag)->once()->getMock()
            ->shouldReceive('build')->with('imgRes')->andReturn($pictureElement)->once();
    }

    public function testRenderInsideOfPictureTag()
    {
        $this->templateVariableContainer
            ->shouldReceive('exists')->with('image_tools_lazy_loading_placeholder')->andReturnFalse()->once()->getMock()
            ->shouldReceive('exists')->with('image_tools_picture_element_builder')->andReturnTrue()->once()->getMock()
            ->shouldReceive('get')->with('image_tools_picture_element_builder')->andReturn(m::mock(PictureElementBuilder::class))->once();

        $this->viewHelper->setArguments([
            'image' => null,
            'absolute' => false,
            'respectFocusArea' => false,
            'cropName' => null,
            'aspectRatio' => null,
            'pixelDensities' => null,
        ]);

        $this->expectException(Exception::class);
        $this->expectExceptionCode(1536408419);
        $this->viewHelper->initializeArgumentsAndRender();
    }

    public function testRenderWithoutSourceElement()
    {
        $this->setUpRenderFirstPart();

        $this->pictureElementBuilder->shouldReceive('hasSourceElementBuilder')->withNoArgs()->andReturnFalse()->once();

        $this->viewHelper->setArguments([
            'image' => null,
            'absolute' => false,
            'respectFocusArea' => false,
            'cropName' => null,
            'aspectRatio' => null,
            'pixelDensities' => null,
        ]);

        $this->expectException(Exception::class);
        $this->expectExceptionCode(1536553842);

        $this->viewHelper->initializeArgumentsAndRender();
    }

    public function testRenderWithoutImgElement()
    {
        $this->setUpRenderFirstPart();

        $this->pictureElementBuilder
            ->shouldReceive('hasSourceElementBuilder')->withNoArgs()->andReturnTrue()->once()->getMock()
            ->shouldReceive('hasImgElementBuilder')->withNoArgs()->andReturnFalse()->once();

        $this->viewHelper->setArguments([
            'image' => null,
            'absolute' => false,
            'respectFocusArea' => false,
            'cropName' => null,
            'aspectRatio' => null,
            'pixelDensities' => null,
        ]);

        $this->expectException(Exception::class);
        $this->expectExceptionCode(1536408429);

        $this->viewHelper->initializeArgumentsAndRender();
    }

    public function testRender()
    {
        $this->setUpRender(true);
        $this->viewHelper->initializeArgumentsAndRender();
    }

    public function testRenderWithPlaceholder()
    {
        $this->setUpRender(false, true);

        $placeholder = m::mock(Placeholder::class);

        $this->templateVariableContainer
            ->shouldReceive('get')->with('image_tools_lazy_loading_placeholder')->andReturn($placeholder)->getMock();

        $this->pictureElementBuilder
            ->shouldReceive('setPlaceholder')->with($placeholder);

        $this->viewHelper->initializeArgumentsAndRender();
    }
}
