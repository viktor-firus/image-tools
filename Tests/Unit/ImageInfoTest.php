<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Tests\Unit;

use Mockery as m;
use Mockery\Mock;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\TestingFramework\Core\BaseTestCase;
use ViktorFirus\ImageTools\ImageInfo;

class ImageInfoTest extends BaseTestCase
{
    use m\Adapter\Phpunit\MockeryPHPUnitIntegration;

    /**
     * @var ImageService|Mock
     */
    protected $imageService;

    /**
     * @var FileInterface|Mock
     */
    protected $file;

    /**
     * @var ImageInfo
     */
    protected $imageInfo;

    /**
     * @var \ViktorFirus\ImageTools\Tests\Unit\Mock
     */
    protected $mock;

    protected function setUp()
    {
        parent::setUp();

        $this->mock = new \ViktorFirus\ImageTools\Tests\Unit\Mock();

        $this->imageService = m::mock(ImageService::class);
        $this->file = $this->mock->file();

        $this->imageInfo = new ImageInfo($this->file);
        $this->imageInfo->injectImageService($this->imageService);
    }

    public function testGetHashIdentifier()
    {
        $this->assertSame('6fb023bd9342359d5a1697339aef2146237e29ac', $this->imageInfo->getHashIdentifier());
    }

    public function testGetWidth()
    {
        $this->assertSame(400, $this->imageInfo->getWidth());
    }

    public function testGetHeight()
    {
        $this->assertSame(300, $this->imageInfo->getHeight());
    }

    public function testGetAlt()
    {
        $this->assertSame('image-alternative', $this->imageInfo->getAlt());
    }

    public function testGetTitle()
    {
        $this->assertSame('image-title', $this->imageInfo->getTitle());
    }

    public function testGetPath()
    {
        $this->assertSame('/file/path', $this->imageInfo->getPath());
    }

    public function testGetRelativeURL()
    {
        $this->imageService->shouldReceive('getImageUri')->with($this->file, false)->andReturn('relative-url');

        $this->assertSame('relative-url', $this->imageInfo->getRelativeURL());
    }

    public function testGetAbsoluteURL()
    {
        $this->imageService->shouldReceive('getImageUri')->with($this->file, true)->andReturn('absolute-url');

        $this->assertSame('absolute-url', $this->imageInfo->getAbsoluteURL());
    }
}
