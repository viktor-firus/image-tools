<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools;

use ByteCube\ImageTools\Image\Image;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\ImageService\ImageServiceInterface;
use ByteCube\ImageTools\StorageInterface;
use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;

class Storage implements StorageInterface
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ImageServiceInterface
     */
    protected $imageService;

    /**
     * @var ResourceStorage
     */
    private $storage;

    /**
     * @var Utility
     */
    private $utility;

    public function __construct(ResourceStorage $storage)
    {
        $this->storage = $storage;
    }

    public function injectObjectManager(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function injectImageService(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function injectUtility(Utility $utility)
    {
        $this->utility = $utility;
    }

    public function hasImage(string $identifier): bool
    {
        return !!$this->storage->hasFile($identifier);
    }

    public function getImage(string $identifier): ImageInterface
    {
        $file = $this->storage->getFile($identifier);

        return $this->wrapFile($file);
    }

    public function saveImage(string $identifier, string $content): ImageInterface
    {
        $dirname = $this->utility->dirname($identifier);
        $basename = $this->utility->basename($identifier);

        /** @var Folder $folder */
        $folder = $this->storage->hasFolder($dirname) ? $this->storage->getFolder($dirname) : $this->storage->createFolder($dirname);
        $file = $folder->hasFile($basename) ? $this->storage->getFile($identifier) : $folder->createFile($basename);

        $file->setContents($content);

        return $this->wrapFile($file);
    }

    public function addImage(string $file, string $target): ImageInterface
    {
        /** @var Folder $folder */
        $folder = $this->storage->hasFolder($target) ? $this->storage->getFolder($target) : $this->storage->createFolder($target);

        $file = $folder->addFile($file, null, DuplicationBehavior::REPLACE);

        return $this->wrapFile($file);
    }

    private function wrapFile(FileInterface $file): ImageInterface
    {
        $imageInfo = $this->objectManager->get(ImageInfo::class, $file);

        return new Image($imageInfo, $file, $this, $this->imageService);
    }
}
