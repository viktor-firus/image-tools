<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\ViewHelpers;

use Exception;

class SourceViewHelper extends AbstractViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'source';

    /**
     * @var mixed
     */
    protected $image;

    /**
     * @var int
     */
    protected $width;

    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerTagAttribute('media', 'string', '');

        $this->registerArgument('image', 'mixed', '', false, null);
        $this->registerArgument('width', 'int', '', false, null);
    }

    public function initialize()
    {
        parent::initialize();

        $this->image = isset($this->arguments['image']) ? $this->arguments['image'] : null;
        $this->width = isset($this->arguments['width']) ? $this->arguments['width'] : null;
    }

    /**
     * @throws Exception
     */
    public function render()
    {
        if (!$this->pictureElementBuilder) {
            throw new Exception('Source tag can only used inside of picture tag.', 1536408441);
        }

        $sourceElementBuilder = $this->pictureElementBuilder->createSourceElementBuilder();
        $this->setParentProperties($sourceElementBuilder);
        if ($this->width !== null) {
            $sourceElementBuilder->setWidth($this->width);
        }
        $sourceElementBuilder->setTag($this->tag);

        if ($this->image) {
            $sourceElementBuilder->setImage($this->imageFactory->create($this->image));
        }

        return '';
    }
}