<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\ViewHelpers;

use ByteCube\ImageTools\Placeholder;
use Exception;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

class LazyViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'div';

    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerUniversalTagAttributes();

        $this->registerArgument('placeholder', 'string', '', false, '#FFFFFF');
        $this->registerArgument('animation', 'string', '', false, null);
    }

    public function initialize()
    {
        parent::initialize();

        if ($this->templateVariableContainer->exists('image_tools_lazy_loading_placeholder')) {
            throw new Exception('Nested lazy loading view helpers not allowed.', 1538290450);
        }
    }

    /**
     * @return string
     */
    public function render(): string
    {
        $this->tag->addAttribute('data-imt-ll', '');
        $classAttribute = $this->tag->getAttribute('class') . ' imt-ll';
        $this->tag->addAttribute('class', $classAttribute);

        $placeholder = new Placeholder($this->arguments['placeholder']);
        $this->templateVariableContainer->add('image_tools_lazy_loading_placeholder', $placeholder);

        $this->addAnimationAttribute();

        $this->tag->setContent($this->renderChildren());

        $this->templateVariableContainer->remove('image_tools_lazy_loading_placeholder');

        return $this->tag->render();
    }

    protected function addAnimationAttribute()
    {
        $animation = $this->arguments['animation'];

        if ($animation) {
            $this->tag->addAttribute('data-imt-ll-animation', $animation);
        }
    }
}
