<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\ViewHelpers;

use ByteCube\ImageTools\ElementBuilder\PictureElementBuilder;
use Exception;

class PictureViewHelper extends AbstractViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'picture';

    /**
     * @var mixed
     */
    protected $image;

    /**
     * @var bool
     */
    protected $absolutePath;

    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerArgument('image', 'mixed', '', true);
        $this->registerArgument('absolute', 'bool', '', false, false);
    }

    public function initialize()
    {
        parent::initialize();

        $this->image = $this->arguments['image'];
        $this->absolutePath = $this->arguments['absolute'];
    }

    public function render(): string
    {
        if ($this->pictureElementBuilder) {
            throw new Exception('Nested picture tags not allowed.', 1536408419);
        }

        $pictureElementBuilder = $this->objectManager->get(PictureElementBuilder::class);
        if ($this->lazyLoading) {
            $pictureElementBuilder->setPlaceholder($this->lazyLoading);
        }

        $this->templateVariableContainer->add('image_tools_picture_element_builder', $pictureElementBuilder);
        $this->renderChildren();

        if (!$pictureElementBuilder->hasSourceElementBuilder()) {
            throw new Exception('Picture tag requires minimum one source tag.', 1536553842);
        }
        if (!$pictureElementBuilder->hasImgElementBuilder()) {
            throw new Exception('Picture tag requires an img tag.', 1536408429);
        }

        $this->templateVariableContainer->remove('image_tools_picture_element_builder');

        $this->setParentProperties($pictureElementBuilder);
        $pictureElementBuilder->setTag($this->tag);

        $pictureElement = $pictureElementBuilder->build($this->imageFactory->create($this->image));

        return $this->absolutePath
            ? $pictureElement->renderAbsolute()
            : $pictureElement->renderRelative();
    }
}
