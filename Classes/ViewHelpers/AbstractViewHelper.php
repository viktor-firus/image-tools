<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\ViewHelpers;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\ElementBuilder\AbstractElementBuilder;
use ByteCube\ImageTools\ElementBuilder\PictureElementBuilder;
use ByteCube\ImageTools\Placeholder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use ViktorFirus\ImageTools\ImageFactory;

abstract class AbstractViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var ImageFactory
     */
    protected $imageFactory;

    /**
     * @var PictureElementBuilder
     */
    protected $pictureElementBuilder;

    /**
     * @var Placeholder
     */
    protected $lazyLoading = null;

    /**
     * @var string
     */
    protected $cropName;

    /**
     * @var AspectRatio
     */
    protected $aspectRatio;

    /**
     * @var array
     */
    protected $pixelDensities;

    /**
     * @var bool
     */
    protected $respectFocusArea;

    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function injectImageFactory(ImageFactory $imageFactory)
    {
        $this->imageFactory = $imageFactory;
    }

    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerUniversalTagAttributes();

        $this->registerArgument('cropName', 'string', '', false, null);
        $this->registerArgument('aspectRatio', 'string', '', false, null);
        $this->registerArgument('pixelDensities', 'string', '', false, '1');
        $this->registerArgument('respectFocusArea', 'bool', '', false, true);
    }

    public function initialize()
    {
        parent::initialize();

        $this->initializePictureElementBuilder();
        $this->initializeLazyLoading();

        $this->cropName = $this->arguments['cropName'];
        $this->aspectRatio = $this->resolveAspectRatio();
        $this->pixelDensities = $this->resolvePixelDensities();
        $this->respectFocusArea = $this->arguments['respectFocusArea'];
    }

    protected function initializePictureElementBuilder()
    {
        if ($this->templateVariableContainer->exists('image_tools_picture_element_builder')) {
            $this->pictureElementBuilder = $this->templateVariableContainer->get('image_tools_picture_element_builder');
        }
    }

    protected function initializeLazyLoading()
    {
        if ($this->templateVariableContainer->exists('image_tools_lazy_loading_placeholder')) {
            $this->lazyLoading = $this->templateVariableContainer->get('image_tools_lazy_loading_placeholder');
        }
    }

    protected function resolveAspectRatio(): ?AspectRatio
    {
        if ($this->arguments['aspectRatio']) {
            $aspectRatio = GeneralUtility::trimExplode('x', (string)$this->arguments['aspectRatio'], true);

            if (count($aspectRatio) === 2) {
                return new AspectRatio((float)$aspectRatio[0], (float)$aspectRatio[1]);
            }
        }

        return null;
    }

    protected function resolvePixelDensities(): ?array
    {
        if ($this->arguments['pixelDensities']) {
            return GeneralUtility::trimExplode(',', (string)$this->arguments['pixelDensities'], true);
        }

        return null;
    }

    protected function setParentProperties(AbstractElementBuilder $elementBuilder)
    {
        $this->setCropNameToElementBuilder($elementBuilder);
        $this->setAspectRatioToElementBuilder($elementBuilder);
        $this->setPixelDensitiesToElementBuilder($elementBuilder);
        $this->setRespectFocusAreaToElementBuilder($elementBuilder);
    }

    protected function setCropNameToElementBuilder(AbstractElementBuilder $elementBuilder)
    {
        if ($this->cropName !== null) {
            $elementBuilder->setCropName($this->cropName);
        }
    }

    protected function setAspectRatioToElementBuilder(AbstractElementBuilder $elementBuilder)
    {
        if ($this->aspectRatio && $this->aspectRatio->isValid()) {
            $elementBuilder->setAspectRatio($this->aspectRatio);
        }
    }

    protected function setPixelDensitiesToElementBuilder(AbstractElementBuilder $elementBuilder)
    {
        if ($this->pixelDensities !== null) {
            $elementBuilder->setPixelDensities($this->pixelDensities);
        }
    }

    protected function setRespectFocusAreaToElementBuilder(AbstractElementBuilder $elementBuilder)
    {
        if ($this->respectFocusArea !== null) {
            $elementBuilder->setRespectFocusArea($this->respectFocusArea);
        }
    }
}
