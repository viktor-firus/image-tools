<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\ViewHelpers;

use ByteCube\ImageTools\ElementBuilder\ImgElementBuilder;

class ImgViewHelper extends AbstractViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'img';

    /**
     * @var mixed
     */
    protected $image;

    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $fallbackWidth;

    /**
     * @var bool
     */
    protected $absolutePath;

    public function initializeArguments()
    {
        parent::initializeArguments();

        $this->registerTagAttribute('alt', 'string', '');

        $this->registerArgument('image', 'mixed', '', false, null);
        $this->registerArgument('width', 'int', '', false, null);
        $this->registerArgument('fallbackWidth', 'int', '', false, null);
        $this->registerArgument('absolute', 'bool', '', false, false);
    }

    public function initialize()
    {
        parent::initialize();

        $this->image = $this->arguments['image'];
        $this->width = $this->arguments['width'];
        $this->fallbackWidth = $this->arguments['fallbackWidth'];
        $this->absolutePath = $this->arguments['absolute'];
    }

    public function render(): string
    {
        $imgElementBuilder = $this->pictureElementBuilder !== null
            ? $this->pictureElementBuilder->createImgElementBuilder()
            : $this->objectManager->get(ImgElementBuilder::class);

        if ($this->lazyLoading) {
            $imgElementBuilder->setPlaceholder($this->lazyLoading);
        }

        $this->setParentProperties($imgElementBuilder);
        if ($this->width !== null) {
            $imgElementBuilder->setWidth($this->width);
        }
        if ($this->fallbackWidth !== null) {
            $imgElementBuilder->setFallbackWidth($this->fallbackWidth);
        }
        $imgElementBuilder->setTag($this->tag);

        if ($this->pictureElementBuilder) {
            if ($this->image !== null) {
                $imgElementBuilder->setImage($this->imageFactory->create($this->image));
            }

            return '';
        }

        $imgElement = $imgElementBuilder->build($this->imageFactory->create($this->image));

        return $this->absolutePath
            ? $imgElement->renderAbsolute()
            : $imgElement->renderRelative();
    }
}