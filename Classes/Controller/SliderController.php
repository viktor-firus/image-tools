<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Controller;

use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use ViktorFirus\ImageTools\Configuration;

/**
 * @codeCoverageIgnore
 */
class SliderController extends AbstractController
{
    /**
     * @param ViewInterface $view
     */
    protected function initializeView(ViewInterface $view)
    {
        parent::initializeView($view);

        $this->addMenuEntry('Overview', 'Slider', 'index');
    }

    public function indexAction()
    {
        $config = Configuration::getConfiguration();

        $this->view->assign('config', $config);
    }
}
