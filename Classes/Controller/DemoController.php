<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Controller;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\ElementBuilder\PictureElementBuilder;
use ByteCube\ImageTools\Image\ImageInterface;
use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use TYPO3\CMS\Core\Resource\Exception\ExistingTargetFileNameException;
use TYPO3\CMS\Core\Resource\Exception\ExistingTargetFolderException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderWritePermissionsException;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use ViktorFirus\ImageTools\ImageFactory;
use ViktorFirus\ImageTools\Slider;
use ViktorFirus\ImageTools\Story\ImageStorySize\RelativeImageStorySize;
use ViktorFirus\ImageTools\Story\StoryCounter;

/**
 * @codeCoverageIgnore
 */
class DemoController extends AbstractController
{
    /**
     * @var StorageRepository
     */
    protected $storageRepository;

    /**
     * @var ImageFactory
     */
    protected $imageFactory;

    /**
     * @var string
     */
    protected $absoluteResourcePath;

    /**
     * @var string
     */
    protected $webResourcePath;

    /**
     * @var Folder
     */
    protected $demoFolder;

    /**
     * @var ResourceStorage
     */
    protected $storage;

    public function injectStorageRepository(StorageRepository $storageRepository)
    {
        $this->storageRepository = $storageRepository;
    }

    public function injectImageFactory(ImageFactory $imageFactory)
    {
        $this->imageFactory = $imageFactory;
    }

    /**
     * @throws ExistingTargetFileNameException
     * @throws ExistingTargetFolderException
     * @throws InsufficientFolderAccessPermissionsException
     * @throws InsufficientFolderWritePermissionsException
     */
    protected function initializeAction()
    {
        parent::initializeAction();
        $this->absoluteResourcePath = ExtensionManagementUtility::extPath('image_tools') . 'Resources/Public/Demo/';
        $this->webResourcePath = '/' . PathUtility::getRelativePath(PATH_site, $this->absoluteResourcePath);
        $this->demoFolder = $this->prepareDemoFolder();
        $this->storage = $this->demoFolder->getStorage();
    }

    /**
     * @param ViewInterface $view
     */
    protected function initializeView(ViewInterface $view)
    {
        parent::initializeView($view);

        $this->addMenuEntry('picture / Crop/Focus/AspectRatio Demo', 'Demo', 'cropFocusAspectRatio');
        $this->addMenuEntry('picture / Crop Demo', 'Demo', 'crop');
        $this->addMenuEntry('img Demo', 'Demo', 'img');
        $this->addMenuEntry('LazyLoad', 'Demo', 'lazyLoad');
        $this->addMenuEntry('URL List', 'Demo', 'urlList');
        $this->addMenuEntry('Slider', 'Demo', 'slider');
    }

    public function cropFocusAspectRatioAction()
    {
        $originalImage = $this->storage->getFileInFolder('brisch27_focus.jpg', $this->demoFolder);
        $fileReferenceData = [
            'uid_local' => (int)$originalImage->getProperty('uid'),
            'crop' => '{"default":{"cropArea":{"height":0.93168168,"width":0.8785,"x":0.0485,"y":0.03678678},"selectedRatio":"NaN","focusArea":{"x":0.66420034,"y":0.05237711,"width":0.16107,"height":0.33924254}}}',
        ];
        $demoFileReference = new FileReference($fileReferenceData);

        $pictureElementBuilder = $this->objectManager->get(PictureElementBuilder::class);
        $pictureElementBuilder->setCropName('default');

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setAspectRatio(new AspectRatio(34, 10));
        $sourceElementBuilder->setWidth(1400);

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setAspectRatio(new AspectRatio(25, 10));
        $sourceElementBuilder->setWidth(1200);

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setAspectRatio(new AspectRatio(16, 10));
        $sourceElementBuilder->setWidth(1000);

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setAspectRatio(new AspectRatio(16, 24));
        $sourceElementBuilder->setWidth(800);

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setAspectRatio(new AspectRatio(16, 38));
        $sourceElementBuilder->setWidth(600);

        $imgElementBuilder = $pictureElementBuilder->createImgElementBuilder();
        $imgElementBuilder->setAspectRatio(new AspectRatio(16, 10));
        $imgElementBuilder->setWidth(500);

        $pictureElement = $pictureElementBuilder->build($this->imageFactory->create($demoFileReference));
        $sourceElements = $pictureElement->getSourceElements()->getElements();

        $singlePictures = [
            [
                'yourSystem' => $sourceElements[0]->getImages()[0],
                'reference' => 'brisch27_focus_34x10.jpg',
                'imgWidth' => 700,
                'description' => '34x10',
            ],
            [
                'yourSystem' => $sourceElements[1]->getImages()[0],
                'reference' => 'brisch27_focus_25x10.jpg',
                'imgWidth' => 600,
                'description' => '25x10',
            ],
            [
                'yourSystem' => $sourceElements[2]->getImages()[0],
                'reference' => 'brisch27_focus_16x10.jpg',
                'imgWidth' => 500,
                'description' => '16x10',
            ],
            [
                'yourSystem' => $sourceElements[3]->getImages()[0],
                'reference' => 'brisch27_focus_16x24.jpg',
                'imgWidth' => 400,
                'description' => '16x24',
            ],
            [
                'yourSystem' => $sourceElements[4]->getImages()[0],
                'reference' => 'brisch27_focus_16x38.jpg',
                'imgWidth' => 300,
                'description' => '16x38',
            ],
            [
                'yourSystem' => $pictureElement->getImgElement()->getImages()[0],
                'reference' => 'brisch27_focus_fallback.jpg',
                'imgWidth' => 250,
                'description' => 'fallback / 16x10',
            ],
        ];

        $singlePictures = $this->prepareSinglePictures($singlePictures);


        $this->view->assignMultiple([
            'demoFileReference' => $demoFileReference,
            'singlePictures' => $singlePictures,
        ]);
    }

    public function cropAction()
    {
        $originalImage = $this->storage->getFileInFolder('mcthilda_crop.jpg', $this->demoFolder);
        $fileReferenceData = [
            'uid_local' => (int)$originalImage->getProperty('uid'),
            'crop' => '{"device1":{"cropArea":{"height":0.953488372093023,"width":0.908,"x":0.046,"y":0.031507876969242},"selectedRatio":"NaN","focusArea":null},"device2":{"cropArea":{"height":0.842460615153788,"width":0.452,"x":0.2835,"y":0.119279819954989},"selectedRatio":"NaN","focusArea":null},"device3":{"cropArea":{"height":0.639159789947487,"width":0.322,"x":0.352,"y":0.296324081020255},"selectedRatio":"NaN","focusArea":null}}',
        ];
        $demoFileReference = new FileReference($fileReferenceData);

        $pictureElementBuilder = $this->objectManager->get(PictureElementBuilder::class);
        $pictureElementBuilder->setCropName('device2');

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setCropName('device1');
        $sourceElementBuilder->setWidth(1400);

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setWidth(1200);

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setCropName('device3');
        $sourceElementBuilder->setWidth(1000);

        $pictureElementBuilder->createImgElementBuilder()->setWidth(700);

        $pictureElement = $pictureElementBuilder->build($this->imageFactory->create($demoFileReference));
        $sourceElements = $pictureElement->getSourceElements()->getElements();

        $singlePictures = [
            [
                'yourSystem' => $sourceElements[0]->getImages()[0],
                'reference' => 'mcthilda_crop_device1.jpg',
                'imgWidth' => 700,
                'description' => 'device1',
            ],
            [
                'yourSystem' => $sourceElements[1]->getImages()[0],
                'reference' => 'mcthilda_crop_device2.jpg',
                'imgWidth' => 600,
                'description' => 'device2',
            ],
            [
                'yourSystem' => $sourceElements[2]->getImages()[0],
                'reference' => 'mcthilda_crop_device3.jpg',
                'imgWidth' => 500,
                'description' => 'device3',
            ],
            [
                'yourSystem' => $pictureElement->getImgElement()->getImages()[0],
                'reference' => 'mcthilda_crop_fallback.jpg',
                'imgWidth' => 350,
                'description' => 'fallback / device2',
            ],
        ];

        $singlePictures = $this->prepareSinglePictures($singlePictures);


        $this->view->assignMultiple([
            'demoFileReference' => $demoFileReference,
            'singlePictures' => $singlePictures,
        ]);
    }

    public function imgAction()
    {
        $originalImage = $this->storage->getFileInFolder('mcthilda_crop.jpg', $this->demoFolder);
        $fileReferenceData = [
            'uid_local' => (int)$originalImage->getProperty('uid'),
            'crop' => '{"device1":{"cropArea":{"height":0.953488372093023,"width":0.908,"x":0.046,"y":0.031507876969242},"selectedRatio":"NaN","focusArea":null},"device2":{"cropArea":{"height":0.842460615153788,"width":0.452,"x":0.2835,"y":0.119279819954989},"selectedRatio":"NaN","focusArea":null},"device3":{"cropArea":{"height":0.639159789947487,"width":0.322,"x":0.352,"y":0.296324081020255},"selectedRatio":"NaN","focusArea":null}}',
        ];
        $demoFileReference = new FileReference($fileReferenceData);

        $pictures = [
            [
                'cropName' => 'device1',
                'description' => 'Picture 1',
                'reference' => $this->webResourcePath . 'mcthilda_crop_device1.jpg',
                'styleWidth' => 500,
            ],
            [
                'cropName' => 'device2',
                'description' => 'Picture 2',
                'reference' => $this->webResourcePath . 'mcthilda_crop_device2.jpg',
                'styleWidth' => 400,
            ],
            [
                'cropName' => 'device3',
                'description' => 'Picture 3',
                'reference' => $this->webResourcePath . 'mcthilda_crop_device3.jpg',
                'styleWidth' => 300,
            ],
        ];

        $this->view->assignMultiple([
            'demoFileReference' => $demoFileReference,
            'pictures' => $pictures,
        ]);
    }

    public function lazyLoadAction()
    {
        $originalImage = $this->storage->getFileInFolder('mcthilda_crop.jpg', $this->demoFolder);
        $fileReferenceData = [
            'uid_local' => (int)$originalImage->getProperty('uid'),
            'crop' => '{"device1":{"cropArea":{"height":0.953488372093023,"width":0.908,"x":0.046,"y":0.031507876969242},"selectedRatio":"NaN","focusArea":null},"device2":{"cropArea":{"height":0.842460615153788,"width":0.452,"x":0.2835,"y":0.119279819954989},"selectedRatio":"NaN","focusArea":null},"device3":{"cropArea":{"height":0.639159789947487,"width":0.322,"x":0.352,"y":0.296324081020255},"selectedRatio":"NaN","focusArea":null}}',
        ];
        $demoFileReference = new FileReference($fileReferenceData);

        $this->view->assignMultiple([
            'demoFileReference' => $demoFileReference,
        ]);
    }

    public function urlListAction()
    {
        $originalImage = $this->storage->getFileInFolder('mcthilda_crop.jpg', $this->demoFolder);
        $fileReferenceData = [
            'uid_local' => (int)$originalImage->getProperty('uid'),
            'crop' => '{"device1":{"cropArea":{"height":0.953488372093023,"width":0.908,"x":0.046,"y":0.031507876969242},"selectedRatio":"NaN","focusArea":null},"device2":{"cropArea":{"height":0.842460615153788,"width":0.452,"x":0.2835,"y":0.119279819954989},"selectedRatio":"NaN","focusArea":null},"device3":{"cropArea":{"height":0.639159789947487,"width":0.322,"x":0.352,"y":0.296324081020255},"selectedRatio":"NaN","focusArea":null}}',
        ];
        $demoFileReference = new FileReference($fileReferenceData);

        $pictureElementBuilder = $this->objectManager->get(PictureElementBuilder::class);
        $pictureElementBuilder->setCropName('device2');

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setCropName('device1');
        $sourceElementBuilder->setWidth(1400);

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setWidth(1200);

        $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();
        $sourceElementBuilder->setCropName('device3');
        $sourceElementBuilder->setWidth(1000);

        $pictureElementBuilder->createImgElementBuilder()->setWidth(700);

        $pictureElement = $pictureElementBuilder->build($this->imageFactory->create($demoFileReference));

        $this->view->assignMultiple([
            'relativeUrls' => json_encode($pictureElement->getRelativeImageURLs(), JSON_PRETTY_PRINT),
            'absoluteUrls' => json_encode($pictureElement->getAbsoluteImageURLs(), JSON_PRETTY_PRINT),
        ]);
    }

    public function sliderAction()
    {
        $slider = new Slider('default', new StoryCounter('demoElement'));

        $image = $this->imageFactory->create($this->storage->getFileInFolder('alabelarde.jpg', $this->demoFolder));
        $story = $slider->createImageStory($image, 1, 6);
        $story->setSize(0, new RelativeImageStorySize(250, 500));
        $action = $story->getAction(1, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(0);
        $opacity = $action->getOpacity();
        $opacity->setValue(0);

        $action = $story->getAction(2, 0);
        $position = $action->getPosition();
        $position->setLeft(0);
        $position->setBottom(0);

        $action = $story->getAction(6, 0);
        $position = $action->getPosition();
        $position->setLeft(1100);
        $position->setBottom(0);


        $story = $slider->createTextStory('<h1 style="text-align: left;">Image #1</h1><p style="text-align: left;">by <a href="https://unsplash.com/@alabelarde">Al Abelarde</a></p>', 1, 9);
        $action = $story->getAction(1, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(1100);

        $action = $story->getAction(2, 0);
        $transition = $action->getTransition();
        $transition->setDelay(500);
        $position = $action->getPosition();
        $position->setLeft(50);
        $position->setTop(0);


        $image = $this->imageFactory->create($this->storage->getFileInFolder('anniespratt.jpg', $this->demoFolder));
        $story = $slider->createImageStory($image, 2, 6);
        $story->setSize(0, new RelativeImageStorySize(250, 500));
        $action = $story->getAction(2, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(0);
        $opacity = $action->getOpacity();
        $opacity->setValue(0);

        $action = $story->getAction(3, 0);
        $position = $action->getPosition();
        $position->setLeft(250);
        $position->setBottom(0);

        $action = $story->getAction(6, 0);
        $position = $action->getPosition();
        $position->setLeft(1100);
        $position->setBottom(0);


        $story = $slider->createTextStory('<h1 style="text-align: left;">Image #2</h1><p style="text-align: left;">by <a href="https://unsplash.com/@anniespratt">Annie Spratt</a></p>', 2, 9);
        $action = $story->getAction(2, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(1100);

        $action = $story->getAction(3, 0);
        $transition = $action->getTransition();
        $transition->setDelay(500);
        $position = $action->getPosition();
        $position->setLeft(50);
        $position->setTop(120);


        $image = $this->imageFactory->create($this->storage->getFileInFolder('brisch27.jpg', $this->demoFolder));
        $story = $slider->createImageStory($image, 3, 6);
        $story->setSize(0, new RelativeImageStorySize(250, 500));
        $action = $story->getAction(3, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(0);
        $opacity = $action->getOpacity();
        $opacity->setValue(0);

        $action = $story->getAction(4, 0);
        $position = $action->getPosition();
        $position->setLeft(500);
        $position->setBottom(0);

        $action = $story->getAction(6, 0);
        $position = $action->getPosition();
        $position->setLeft(1100);
        $position->setBottom(0);


        $story = $slider->createTextStory('<h1 style="text-align: left;">Image #3</h1><p style="text-align: left;">by <a href="https://unsplash.com/@brisch27">Brigitta Schneiter</a></p>', 3, 9);
        $action = $story->getAction(3, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(1100);

        $action = $story->getAction(4, 0);
        $transition = $action->getTransition();
        $transition->setDelay(500);
        $position = $action->getPosition();
        $position->setLeft(50);
        $position->setTop(240);


        $image = $this->imageFactory->create($this->storage->getFileInFolder('ettocl.jpg', $this->demoFolder));
        $story = $slider->createImageStory($image, 4, 6);
        $story->setSize(0, new RelativeImageStorySize(250, 500));
        $action = $story->getAction(4, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(0);
        $opacity = $action->getOpacity();
        $opacity->setValue(0);

        $action = $story->getAction(5, 0);
        $position = $action->getPosition();
        $position->setLeft(750);
        $position->setBottom(0);

        $action = $story->getAction(6, 0);
        $position = $action->getPosition();
        $position->setLeft(1100);
        $position->setBottom(0);


        $story = $slider->createTextStory('<h1 style="text-align: left;">Image #4</h1><p style="text-align: left;">by <a href="https://unsplash.com/@ettocl">Léonard Cotte</a></p>', 4, 9);
        $action = $story->getAction(4, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(1100);

        $action = $story->getAction(5, 0);
        $transition = $action->getTransition();
        $transition->setDelay(500);
        $position = $action->getPosition();
        $position->setLeft(50);
        $position->setTop(360);


        $image = $this->imageFactory->create($this->storage->getFileInFolder('mcthilda.jpg', $this->demoFolder));
        $story = $slider->createImageStory($image, 5, 9);
        $story->setSize(0, new RelativeImageStorySize(250, 500));
        $action = $story->getAction(5, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(0);
        $opacity = $action->getOpacity();
        $opacity->setValue(0);

        $action = $story->getAction(6, 0);
        $position = $action->getPosition();
        $position->setLeft(0);
        $position->setBottom(0);


        $story = $slider->createTextStory('<h1 style="text-align: left;">Image #5</h1><p style="text-align: left;">by <a href="https://unsplash.com/@mcthilda">Mathilda Khoo</a></p>', 5, 9);
        $action = $story->getAction(5, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(1100);

        $action = $story->getAction(6, 0);
        $transition = $action->getTransition();
        $transition->setDelay(500);
        $position = $action->getPosition();
        $position->setLeft(250);
        $position->setTop(0);


        $image = $this->imageFactory->create($this->storage->getFileInFolder('micheile.jpg', $this->demoFolder));
        $story = $slider->createImageStory($image, 6, 9);
        $story->setSize(0, new RelativeImageStorySize(250, 500));
        $action = $story->getAction(6, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(0);
        $opacity = $action->getOpacity();
        $opacity->setValue(0);

        $action = $story->getAction(7, 0);
        $position = $action->getPosition();
        $position->setLeft(250);
        $position->setBottom(0);


        $story = $slider->createTextStory('<h1 style="text-align: left;">Image #6</h1><p style="text-align: left;">by <a href="https://unsplash.com/@micheile">Micheile Henderson</a></p>', 6, 9);
        $action = $story->getAction(6, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(1100);

        $action = $story->getAction(7, 0);
        $transition = $action->getTransition();
        $transition->setDelay(500);
        $position = $action->getPosition();
        $position->setLeft(250);
        $position->setTop(120);


        $image = $this->imageFactory->create($this->storage->getFileInFolder('samimatias.jpg', $this->demoFolder));
        $story = $slider->createImageStory($image, 7, 9);
        $story->setSize(0, new RelativeImageStorySize(250, 500));
        $action = $story->getAction(7, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(0);
        $opacity = $action->getOpacity();
        $opacity->setValue(0);

        $action = $story->getAction(8, 0);
        $position = $action->getPosition();
        $position->setLeft(500);
        $position->setBottom(0);


        $story = $slider->createTextStory('<h1 style="text-align: left;">Image #7</h1><p style="text-align: left;">by <a href="https://unsplash.com/@samimatias">Sami Takarautio</a></p>', 7, 9);
        $action = $story->getAction(7, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(1100);

        $action = $story->getAction(8, 0);
        $transition = $action->getTransition();
        $transition->setDelay(500);
        $position = $action->getPosition();
        $position->setLeft(250);
        $position->setTop(240);


        $image = $this->imageFactory->create($this->storage->getFileInFolder('sydneyangove.jpg', $this->demoFolder));
        $story = $slider->createImageStory($image, 8, 9);
        $story->setSize(0, new RelativeImageStorySize(250, 500));
        $action = $story->getAction(8, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(0);
        $opacity = $action->getOpacity();
        $opacity->setValue(0);

        $action = $story->getAction(9, 0);
        $position = $action->getPosition();
        $position->setLeft(750);
        $position->setBottom(0);


        $story = $slider->createTextStory('<h1 style="text-align: left;">Image #8</h1><p style="text-align: left;">by <a href="https://unsplash.com/@sydneyangove">Sydney Angove</a></p>', 8, 9);
        $action = $story->getAction(8, 0);
        $position = $action->getPosition();
        $position->setRight(1100);
        $position->setBottom(1100);

        $action = $story->getAction(9, 0);
        $transition = $action->getTransition();
        $transition->setDelay(500);
        $position = $action->getPosition();
        $position->setLeft(250);
        $position->setTop(360);


        $slider->setDuration(1, 200);
        $slider->setDuration(2, 2000);
        $slider->setDuration(3, 2000);
        $slider->setDuration(4, 2000);
        $slider->setDuration(5, 2000);
        $slider->setDuration(6, 2000);
        $slider->setDuration(7, 2000);
        $slider->setDuration(8, 2000);
        $slider->setDuration(9, 2000);

        $this->view->assign('content', $slider->render());
    }

    /**
     * @param array $singlePictures
     *
     * @return array
     */
    protected function prepareSinglePictures(array $singlePictures): array
    {
        foreach ($singlePictures as &$singlePicture) {
            /** @var ImageInterface $image */
            $image = $singlePicture['yourSystem'];

            $singlePicture['yourSystem'] = $image->getImageInfo()->getAbsoluteURL();
            $singlePicture['reference'] = $this->webResourcePath . $singlePicture['reference'];
        }

        return $singlePictures;
    }

    /**
     * @return Folder
     * @throws ExistingTargetFileNameException
     * @throws ExistingTargetFolderException
     * @throws InsufficientFolderAccessPermissionsException
     * @throws InsufficientFolderWritePermissionsException
     */
    protected function prepareDemoFolder(): Folder
    {
        /** @var ResourceStorage $storage */
        $storage = current($this->storageRepository->findAll());

        $imageToolsFolderName = 'image-tools';
        if (!$storage->hasFolder($imageToolsFolderName)) {
            $storage->createFolder($imageToolsFolderName);
        }
        $imageToolsFolder = $storage->getFolder($imageToolsFolderName);

        $demoFolderName = 'demo';
        if (!$imageToolsFolder->hasFolder($demoFolderName)) {
            $imageToolsFolder->createFolder($demoFolderName);
        }
        $demoFolder = $storage->getFolder($imageToolsFolder->getIdentifier() . $demoFolderName);

        $demoFiles = [
            'mcthilda_crop.jpg',
            'brisch27_focus.jpg',
            'alabelarde.jpg',
            'anniespratt.jpg',
            'brisch27.jpg',
            'ettocl.jpg',
            'mcthilda.jpg',
            'micheile.jpg',
            'samimatias.jpg',
            'sydneyangove.jpg',
        ];
        foreach ($demoFiles as $demoFile) {
            $this->copyDemoFile($storage, $demoFolder, $demoFile);
        }

        return $demoFolder;
    }

    /**
     * @param ResourceStorage $storage
     * @param Folder          $targetFolder
     * @param string          $demoFile
     *
     * @throws ExistingTargetFileNameException
     */
    protected function copyDemoFile(ResourceStorage $storage, Folder $targetFolder, string $demoFile)
    {
        if (!$targetFolder->hasFile($demoFile)) {
            $demoImagePath = ExtensionManagementUtility::extPath('image_tools') . 'Resources/Private/Demo/' . $demoFile;
            $storage->addFile($demoImagePath, $targetFolder, $demoFile, DuplicationBehavior::REPLACE, false);
        }
    }
}
