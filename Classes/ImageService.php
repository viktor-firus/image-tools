<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools;

use ByteCube\ImageTools\Crop\CropDimensions;
use ByteCube\ImageTools\Image\Image;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\ImageService\AbstractImageService;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\CommandUtility;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;

class ImageService extends AbstractImageService implements SingletonInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \TYPO3\CMS\Extbase\Service\ImageService
     */
    protected $imageService;

    /**
     * @var Utility
     */
    protected $utility;

    /**
     * @var CommandUtility
     */
    protected $commandUtility;

    public function injectObjectManager(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function injectImageService(\TYPO3\CMS\Extbase\Service\ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function injectUtility(Utility $utility)
    {
        $this->utility = $utility;
    }

    public function injectCommandUtility(CommandUtility $commandUtility)
    {
        $this->commandUtility = $commandUtility;
    }

    public function crop(
        ImageInterface $image,
        ?int $maxWidth,
        float $pixelDensity,
        CropDimensions $cropDimensions
    ): ImageInterface {
        $imageServiceInstructions = $this->generateImageServiceInstructions($maxWidth, $cropDimensions);

        $processedFile = $this->imageService->applyProcessingInstructions(
            $image->getOriginal(),
            $imageServiceInstructions
        );

        $imageInfo = $this->objectManager->get(ImageInfo::class, $processedFile);
        $storage = $this->objectManager->get(Storage::class, $processedFile->getStorage());

        return new Image($imageInfo, $processedFile, $storage, $this, $pixelDensity);
    }

    public function blur(ImageInterface $image): ImageInterface
    {
        $targetFileName = $image->getImageInfo()->getHashIdentifier() . '.jpg';
        $targetFolder = 'image-tools/placeholder/blurred';

        $fileIdentifier = $targetFolder . '/' . $targetFileName;
        if ($image->getStorage()->hasImage($fileIdentifier)) {
            return $image->getStorage()->getImage($fileIdentifier);
        }

        $tempFolder = PATH_site . 'typo3temp/ImageTools/';
        if (!is_dir($tempFolder)) {
            $this->utility->mkdirDeep($tempFolder);
        }

        $imagePath = $image->getImageInfo()->getPath();

        $params = [
            '-resize ' . $this->commandUtility->escapeShellArgument('25%'),
            '-gaussian-blur ' . $this->commandUtility->escapeShellArgument('0x8'),
            '-quality 60',
            $this->commandUtility->escapeShellArgument($imagePath),
            $this->commandUtility->escapeShellArgument($tempFolder . $targetFileName),
        ];

        $command = $this->commandUtility->imageMagickCommand('convert', implode(' ', $params));
        $this->commandUtility->exec($command);

        return $image->getStorage()->addImage($tempFolder . $targetFileName, $targetFolder);
    }

    protected function generateImageServiceInstructions(?int $maxWidth, CropDimensions $cropDimensions): array
    {
        return [
            'width' => null,
            'height' => null,
            'minWidth' => null,
            'minHeight' => null,
            'maxWidth' => $maxWidth,
            'maxHeight' => null,
            'crop' => $this->generateCropJson($cropDimensions),
        ];
    }

    protected function generateCropJson(CropDimensions $dimensions): string
    {
        return json_encode([
            'x' => $dimensions->getX(),
            'y' => $dimensions->getY(),
            'width' => $dimensions->getWidth(),
            'height' => $dimensions->getHeight(),
            'rotate' => 0,
        ]);
    }
}
