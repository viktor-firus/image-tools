<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Story;

use ViktorFirus\ImageTools\Configuration;
use ViktorFirus\ImageTools\CssBuilder;
use ViktorFirus\ImageTools\TagBuilder;
use Webmozart\Assert\Assert;

class TextStory extends AbstractStory
{
    /**
     * @var string
     */
    private $content;

    /**
     * @var int[]
     */
    private $widths = [];

    public function __construct(
        string $content,
        string $storyId,
        string $configKey,
        int $firstSlide,
        int $lastSlide
    ) {
        parent::__construct($storyId, $configKey, $firstSlide, $lastSlide);

        $this->content = $content;
    }

    public function setWidth(int $breakpoint, int $width): void
    {
        Assert::range($width, 0, 1000);
        Configuration::assertBreakpointExists($this->configKey, $breakpoint);

        $this->widths[$breakpoint] = $width;
    }

    public function renderStyle(CssBuilder $builder = null): CssBuilder
    {
        $builder = $builder ?: new CssBuilder();

        foreach ($this->widths as $breakpoint => $width) {
            $builder->add('width:' . ($width / 10) . '%;', $breakpoint, ['div#' . $this->storyId]);
        }

        return parent::renderStyle($builder);
    }

    public function renderElement(): TagBuilder
    {
        $storyElement = parent::renderElement();
        $storyElement->setContent($this->content);

        return $storyElement;
    }
}
