<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Story;

use ByteCube\ImageTools\AspectRatio;
use ByteCube\ImageTools\ElementBuilder\PictureElementBuilder;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\Placeholder;
use ViktorFirus\ImageTools\Configuration;
use ViktorFirus\ImageTools\CssBuilder;
use ViktorFirus\ImageTools\Story\ImageStorySize\ImageStorySizeInterface;
use ViktorFirus\ImageTools\Story\ImageStorySize\RelativeImageStorySize;
use ViktorFirus\ImageTools\TagBuilder;

class ImageStory extends AbstractStory
{
    /**
     * @var ImageInterface
     */
    private $image;

    /**
     * @var ImageStorySizeInterface[]
     */
    private $sizes;

    public function __construct(
        ImageInterface $image,
        string $storyId,
        string $configKey,
        int $firstSlide,
        int $lastSlide
    ) {
        parent::__construct($storyId, $configKey, $firstSlide, $lastSlide);

        $this->image = $image;
    }

    public function setSize(int $breakpoint, ImageStorySizeInterface $imageStorySize): void
    {
        Configuration::assertBreakpointExists($this->configKey, $breakpoint);

        $this->sizes[$breakpoint] = $imageStorySize;
    }

    public function renderStyle(CssBuilder $builder = null): CssBuilder
    {
        $builder = $builder ?: new CssBuilder();

        foreach ($this->getSizes() as $breakpoint => $size) {
            $builder->add($size->getStyle(), $breakpoint, ['div#' . $this->storyId]);
        }

        return parent::renderStyle($builder);
    }

    public function renderElement(): TagBuilder
    {
        $storyElement = parent::renderElement();
        $storyElement->addClasses('imt-slider-image-story');

        $pictureElementBuilder = new PictureElementBuilder();
        $pictureElementBuilder->setPlaceholder(new Placeholder('blurred'));
        $pictureElementBuilder->setPixelDensities([1, 2, 3]);
        $pictureElementBuilder->setCropName('default');

        $config = Configuration::getSliderConfig($this->configKey);

        $breakpoints = array_keys($config);
        krsort($breakpoints);

        $sizes = $this->getSizes();
        /** @var ImageStorySizeInterface[] $sizes */
        $sizes = array_reverse($sizes, true);

        foreach ($sizes as $breakpoint => $size) {
            $sourceElementBuilder = $pictureElementBuilder->createSourceElementBuilder();

            if ($breakpoint > 0) {
                $tag = new TagBuilder('source');
                $tag->addAttribute('media', '(min-width: ' . $breakpoint . 'px)');
                $sourceElementBuilder->setTag($tag);
            }

            $sourceElementBuilder->setAspectRatio($size->getAspectRatio());

            $baseWidth = $config[$breakpoint]['baseWidth'];
            $sourceElementBuilder->setWidth($size->calculateWidthByBaseWidth($baseWidth));
        }

        $baseWidth = $config[0]['baseWidth'];

        $imageElementBuilder = $pictureElementBuilder->createImgElementBuilder();
        $imageElementBuilder->setAspectRatio(new AspectRatio(1, 1));
        $imageElementBuilder->setWidth($sizes[0]->calculateWidthByBaseWidth($baseWidth));
        $imageElementBuilder->setCropName('device1');

        $pictureElement = $pictureElementBuilder->build($this->image);

        $imageContainer = new TagBuilder('div');
        $imageContainer->addClasses('imt-ll');
        $imageContainer->setContent($pictureElement->renderAbsolute());

        $storyElement->setContent($imageContainer->render());

        return $storyElement;
    }

    /**
     * @return ImageStorySizeInterface[]
     */
    private function getSizes(): array
    {
        $sizes = $this->sizes;
        if (!$sizes) {
            $sizes[0] = new RelativeImageStorySize(250, 250);
        }

        $config = Configuration::getSliderConfig($this->configKey);
        $breakpoints = array_keys($config);

        $result = [];
        $previousSize = null;
        foreach ($breakpoints as $breakpoint) {
            $size = isset($sizes[$breakpoint]) ? $sizes[$breakpoint] : $previousSize;

            $result[$breakpoint] = $size;
            $previousSize = $size;
        }

        return $result;
    }
}
