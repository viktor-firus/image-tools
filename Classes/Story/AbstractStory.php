<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Story;

use ViktorFirus\ImageTools\Configuration;
use ViktorFirus\ImageTools\CssBuilder;
use ViktorFirus\ImageTools\TagBuilder;
use Webmozart\Assert\Assert;

abstract class AbstractStory implements StoryInterface
{
    /**
     * @var string
     */
    protected $storyId;

    /**
     * @var string
     */
    protected $configKey;

    /**
     * @var int
     */
    protected $firstSlide;
    /**
     * @var int
     */
    protected $lastSlide;

    /**
     * @var string
     */
    protected $cssClass = '';

    /**
     * @var Action[][]
     */
    protected $actions = [];

    public function __construct(string $storyId, string $configKey, int $firstSlide, int $lastSlide)
    {
        $this->storyId = $storyId;
        $this->configKey = $configKey;
        $this->firstSlide = $firstSlide;
        $this->lastSlide = $lastSlide;


        Assert::greaterThan($firstSlide, 0, '$firstSlide should be greater than 0.');
        Assert::greaterThan($lastSlide, $firstSlide, '$lastSlide should be greater than $firstSlide.');

        $this->actions[$firstSlide][0] = new Action();
    }

    public function getAction(int $slide, int $breakpoint): Action
    {
        Assert::range(
            $slide,
            $this->firstSlide,
            $this->lastSlide,
            'Slide should be in range of firstSlide and lastSlide.'
        );

        Configuration::assertBreakpointExists($this->configKey, $breakpoint);

        if (!isset($this->actions[$slide][$breakpoint])) {
            $this->actions[$slide][$breakpoint] = new Action();

            ksort($this->actions);
            ksort($this->actions[$slide]);
        }

        return $this->actions[$slide][$breakpoint];
    }

    public function setCssClass(string $cssClass): void
    {
        $this->cssClass = $cssClass;
    }

    public function renderStyle(CssBuilder $builder = null): CssBuilder
    {
        $builder = $builder ?: new CssBuilder();

        foreach ($this->actions as $slide => $slideActions) {
            foreach ($slideActions as $breakpoint => $action) {
                $action->addStyle($builder, $this->storyId, $breakpoint, $slide);
            }
        }

        $builder->add('display:none !important;', 0, ['div.imt-slide-' . ($this->lastSlide + 1), 'div#' . $this->storyId]);

        return $builder;
    }

    public function renderElement(): TagBuilder
    {
        $storyElement = new TagBuilder('div');
        $storyElement->forceClosingTag(true);
        $storyElement->addClasses('imt-slider-story');
        $storyElement->addAttributes([
            'id' => $this->storyId,
        ]);

        return $storyElement;
    }
}
