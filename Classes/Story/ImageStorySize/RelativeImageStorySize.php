<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Story\ImageStorySize;

class RelativeImageStorySize extends AbsoluteImageStorySize
{
    public function getStyle(): string
    {
        return 'width:' . ($this->width / 10) . '%; height: ' . ($this->height / 10) . '%;';
    }

    public function calculateWidthByBaseWidth(int $baseWidth): int
    {
        return (int)($baseWidth * ($this->width / 10 / 100));
    }
}
