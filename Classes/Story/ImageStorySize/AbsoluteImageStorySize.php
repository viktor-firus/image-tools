<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Story\ImageStorySize;

use ByteCube\ImageTools\AspectRatio;
use Webmozart\Assert\Assert;

class AbsoluteImageStorySize implements ImageStorySizeInterface
{
    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    public function __construct(int $width, int $height)
    {
        Assert::greaterThan($width, 0);
        Assert::greaterThan($height, 0);

        $this->width = $width;
        $this->height = $height;
    }

    public function getAspectRatio(): AspectRatio
    {
        return new AspectRatio($this->width, $this->height);
    }

    public function getStyle(): string
    {
        return 'width:' . $this->width . 'px; height: ' . $this->height . 'px;';
    }

    public function calculateWidthByBaseWidth(int $baseWidth): int
    {
        return $this->width;
    }
}
