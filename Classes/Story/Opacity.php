<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Story;

use Webmozart\Assert\Assert;

class Opacity
{
    /**
     * @var float
     */
    private $value = 1.0;

    public function getValue(): float
    {
        return $this->value;
    }

    public function setValue(float $value): void
    {
        Assert::greaterThanEq($value, 0);
        Assert::lessThanEq($value, 1);
        $this->value = $value;
    }

    public function createCss(): string
    {
        return 'opacity:' . $this->value . ';';
    }
}
