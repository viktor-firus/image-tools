<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Story;

class Position
{
    /**
     * @var int
     */
    private $left = 0;

    /**
     * @var int
     */
    private $translateX = 0;

    /**
     * @var int
     */
    private $top = 0;

    /**
     * @var int
     */
    private $translateY = 0;

    public function getValues(): array
    {
        return [
            $this->left,
            $this->translateX,
            $this->top,
            $this->translateY,
        ];
    }

    public function setLeft(int $left): void
    {
        $this->left = $left;
        $this->translateX = 0;
    }

    public function setRight(int $right): void
    {
        $this->left = 1000 - $right;
        $this->translateX = -100;
    }

    public function centerHorizontal(): void
    {
        $this->setLeft(500);
        $this->translateX = -50;
    }

    public function setTop(int $top): void
    {
        $this->top = $top;
        $this->translateY = 0;
    }

    public function setBottom(int $bottom): void
    {
        $this->top = 1000 - $bottom;
        $this->translateY = -100;
    }

    public function centerVertical(): void
    {
        $this->setTop(500);
        $this->translateY = -50;
    }

    public function createCss(): string
    {
        return 'left:' . ($this->left / 10) . '%;' .
            'top:' . ($this->top / 10) . '%;' .
            'transform:translate(' . $this->translateX . '%,' . $this->translateY . '%' . ');';
    }
}
