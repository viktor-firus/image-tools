<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Story;

use Webmozart\Assert\Assert;

class Transition
{
    const LINEAR = 'linear';

    const EASE = 'ease';

    const EASE_IN = 'ease-in';

    const EASE_OUT = 'ease-out';

    const EASE_IN_OUT = 'ease-in-out';

    /**
     * @var int
     */
    private $delay = 0;

    /**
     * @var int
     */
    private $duration = 500;

    /**
     * @var string
     */
    private $transitionTimingFunction = self::LINEAR;

    /**
     * @var int
     */
    private $reverseDelay = null;

    /**
     * @var int
     */
    private $reverseDuration = null;

    /**
     * @var string
     */
    private $reverseTransitionTimingFunction = null;

    public function setDelay(int $delay): void
    {
        Assert::greaterThanEq($delay, 0);

        $this->delay = $delay;
    }

    public function setDuration(int $duration): void
    {
        Assert::greaterThanEq($duration, 0);

        $this->duration = $duration;
    }

    public function setTransitionTimingFunction(string $transitionTimingFunction): void
    {
        $this->assertTimingFunction($transitionTimingFunction);

        $this->transitionTimingFunction = $transitionTimingFunction;
    }

    public function setReverseDelay(int $delay): void
    {
        Assert::greaterThanEq($delay, 0);

        $this->reverseDelay = $delay;
    }

    public function setReverseDuration(int $duration): void
    {
        Assert::greaterThanEq($duration, 0);

        $this->reverseDuration = $duration;
    }

    public function setReverseTransitionTimingFunction(string $transitionTimingFunction): void
    {
        $this->assertTimingFunction($transitionTimingFunction);

        $this->reverseTransitionTimingFunction = $transitionTimingFunction;
    }

    public function getCompleteDuration(): int
    {
        return $this->delay + $this->duration;
    }

    public function getCompleteReverseDuration(): int
    {
        $delay = $this->reverseDelay !== null ? $this->reverseDelay : $this->delay;
        $duration = $this->reverseDuration !== null ? $this->reverseDuration : $this->duration;

        return $delay + $duration;
    }

    public function createTransition(): string
    {
        $generator = function ($property) {
            return $this->generate($property, $this->duration, $this->transitionTimingFunction, $this->delay);
        };

        return 'transition: ' . $generator('top') . ',' . $generator('left') . ',' . $generator('transform') . ',' . $generator('opacity');
    }

    public function createReverseTransition(): string
    {
        $delay = $this->reverseDelay !== null ? $this->reverseDelay : $this->delay;
        $duration = $this->reverseDuration !== null ? $this->reverseDuration : $this->duration;
        $transitionTimingFunction = $this->reverseTransitionTimingFunction !== null ? $this->reverseTransitionTimingFunction : $this->transitionTimingFunction;

        $generator = function ($property) use ($delay, $duration, $transitionTimingFunction) {
            return $this->generate($property, $duration, $transitionTimingFunction, $delay);
        };

        return 'transition: ' . $generator('top') . ',' . $generator('left') . ',' . $generator('transform') . ',' . $generator('opacity');
    }

    private function generate(string $property, int $duration, string $transitionTimingFunction, int $delay)
    {
        return $property . ' ' . $duration . 'ms ' . $transitionTimingFunction . ' ' . $delay . 'ms';
    }

    private function assertTimingFunction(string $timingFunction)
    {
        Assert::oneOf($timingFunction, [self::LINEAR, self::EASE, self::EASE_IN, self::EASE_OUT, self::EASE_IN_OUT]);
    }
}
