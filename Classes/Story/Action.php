<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools\Story;

use ViktorFirus\ImageTools\CssBuilder;

class Action
{
    /**
     * @var Position
     */
    private $position;

    /**
     * @var Transition
     */
    private $transition;

    /**
     * @var Opacity
     */
    private $opacity;

    public function __construct()
    {
        $this->position = new Position();
        $this->transition = new Transition();
        $this->opacity = new Opacity();
    }

    public function getPosition(): Position
    {
        return $this->position;
    }

    public function getTransition(): Transition
    {
        return $this->transition;
    }

    public function getOpacity(): Opacity
    {
        return $this->opacity;
    }

    public function addStyle(CssBuilder $builder, string $storyId, int $breakpoint, int $slide): CssBuilder
    {
        $transition = $this->transition->createTransition();
        $reverseTransition = $this->transition->createReverseTransition();

        $builder->add($transition, $breakpoint, ['div.imt-slide-' . ($slide - 1) . '-' . $slide, 'div#' . $storyId]);
        $builder->add($reverseTransition, $breakpoint, ['div.imt-slide-' . $slide . '-' . ($slide - 1), 'div#' . $storyId]);

        $style = 'display:block;' . $this->position->createCss() . $this->opacity->createCss();
        $builder->add($style, $breakpoint, ['div.imt-slide-' . $slide, 'div#' . $storyId]);

        return $builder;
    }
}
