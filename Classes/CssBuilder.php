<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools;

class CssBuilder
{
    /**
     * @var array
     */
    private $content = [];

    /**
     * @param string   $content
     * @param int      $minWidth
     * @param string[] $selector
     */
    public function add(string $content, int $minWidth, array $selector)
    {
        $selector = array_map(function ($item) {
            return trim($item);
        }, $selector);

        $this->content[$minWidth][implode(' ', $selector)][] = $content;
    }

    public function render(): string
    {
        $result = '';
        foreach ($this->content as $minWidth => $minWidthContent) {
            if ($minWidth > 0) {
                $result .= '@media (min-width: ' . $minWidth . 'px){' . PHP_EOL;
            }

            foreach ($minWidthContent as $selector => $content) {
                $result .= $selector . '{' . implode(' ', $content) . '}' . PHP_EOL;
            }

            if ($minWidth > 0) {
                $result .= '}' . PHP_EOL;
            }
        }

        return $result;
    }
}
