<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools;

use ByteCube\ImageTools\Image\ImageInterface;
use ViktorFirus\ImageTools\Story\ImageStory;
use ViktorFirus\ImageTools\Story\StoryCounterInterface;
use ViktorFirus\ImageTools\Story\StoryInterface;
use ViktorFirus\ImageTools\Story\TextStory;
use Webmozart\Assert\Assert;

class Slider
{
    private const LEFT_BUTTON = 1;

    private const RIGHT_BUTTON = 2;

    /**
     * @var string
     */
    private $configKey;

    /**
     * @var StoryCounterInterface
     */
    private $storyCounter;

    /**
     * @var string
     */
    private $navigation;

    /**
     * @var string
     */
    private $slideIndicator;

    /**
     * @var string
     */
    private $timeoutIndicator;

    /**
     * @var StoryInterface[]
     */
    private $stories;

    /**
     * @var int
     */
    private $slides = 1;

    /**
     * @var int[]
     */
    private $durations;

    public function __construct(
        string $configKey,
        StoryCounterInterface $storyCounter,
        string $navigation = 'hover',
        string $slideIndicator = 'hover',
        string $timeoutIndicator = 'hover'
    ) {
        $this->configKey = $configKey;
        $this->storyCounter = $storyCounter;

        Assert::oneOf($navigation, ['off', 'hover', 'permanent']);
        Assert::oneOf($slideIndicator, ['off', 'hover', 'permanent']);
        Assert::oneOf($timeoutIndicator, ['off', 'hover', 'permanent']);

        $this->navigation = $navigation;
        $this->slideIndicator = $slideIndicator;
        $this->timeoutIndicator = $timeoutIndicator;
    }

    public function createTextStory(string $content, int $firstSlide, int $lastSlide): TextStory
    {
        $story = new TextStory($content, $this->storyCounter->createId(), $this->configKey, $firstSlide, $lastSlide);
        $this->stories[] = $story;

        if ($lastSlide > $this->slides) {
            $this->slides = $lastSlide;
        }

        return $story;
    }

    public function createImageStory(ImageInterface $image, int $firstSlide, int $lastSlide): ImageStory
    {
        $story = new ImageStory($image, $this->storyCounter->createId(), $this->configKey, $firstSlide, $lastSlide);
        $this->stories[] = $story;

        if ($lastSlide > $this->slides) {
            $this->slides = $lastSlide;
        }

        return $story;
    }

    public function setDuration(int $slide, int $duration): void
    {
        Assert::greaterThanEq($duration, 0);
        Assert::range($slide, 1, $this->slides, 'Slide ' . $slide . ' not exist.');

        $this->durations[$slide] = $duration;
    }

    public function render(): string
    {
        $sliderElement = new TagBuilder('div');
        $sliderElement->addClasses('imt-slider', 'imt-slide-1');
        $sliderElement->addAttributes([
            'data-imt-slider' => '',
            'data-slides' => $this->slides,
            'data-durations' => $this->generateDurations(),
        ]);

        $stories = '';
        foreach ($this->stories as $story) {
            $stories .= $story->renderElement()->render();
        }

        $sliderElement->setContent(
            ($this->navigation !== 'off' ? $this->createNavigationButton(self::LEFT_BUTTON) : '') .
            $stories .
            ($this->navigation !== 'off' ? $this->createNavigationButton(self::RIGHT_BUTTON) : '') .
            ($this->slideIndicator !== 'off' ? $this->createSlideIndicator() : '') .
            ($this->timeoutIndicator !== 'off' ? $this->createTimeoutIndicator() : '')
        );

        return $this->generateCss() . $sliderElement->render();
    }

    private function generateDurations(): string
    {
        $durations = $this->durations;
        for ($x = 1; $x <= $this->slides; $x++) {
            $durations[$x] = isset($durations[$x]) ? $durations[$x] : 3500;
        }
        ksort($durations);

        return implode(',', $durations);
    }

    private function generateCss(): string
    {
        $builder = new CssBuilder();

        foreach ($this->stories as $story) {
            $story->renderStyle($builder);
        }

        $style = new TagBuilder('style');
        $style->forceClosingTag(true);
        $style->addAttribute('type', 'text/css');
        $style->setContent($builder->render());

        return $style->render();
    }

    private function createNavigationButton(int $position): string
    {
        $button = new TagBuilder('div');
        $button->addClasses('imt-slider-button', $this->getNavigationButtonClass($position), $this->navigation);
        $button->setContent($this->getNavigationButtonIcon($position));

        return $button->render();
    }

    private function getNavigationButtonClass(int $position): string
    {
        return $position === self::LEFT_BUTTON
            ? 'imt-slider-left-button'
            : 'imt-slider-right-button';
    }

    private function getNavigationButtonIcon(int $position): string
    {
        return $position === self::LEFT_BUTTON
            ? '<svg viewBox="0 0 73 100" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M63.606.012c1.806.183 2.296.386 3.236.791 3.244 1.395 5.63 4.588 6.022 8.125.055.493.048.618.062 1.113v80.023c-.063 2.282-.362 3.03-.974 4.318-2.453 5.162-9.718 7.374-14.696 4.025-.153-.103-.303-.215-.453-.324L3.92 58.072c-1.618-1.297-1.976-1.858-2.602-2.923-2.271-3.863-1.557-9.253 1.744-12.388.363-.345.468-.415.858-.728L56.803 2.022a10.708 10.708 0 016.803-2.01z"/></svg>'
            : '<svg viewBox="0 0 73 100" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M9.32.012C7.514.195 7.024.398 6.084.803 2.84 2.198.455 5.391.062 8.928c-.055.493-.048.618-.061 1.113v80.023c.063 2.282.361 3.03.973 4.318 2.454 5.162 9.718 7.374 14.696 4.025.154-.103.303-.215.454-.324l52.882-40.011c1.618-1.297 1.976-1.858 2.601-2.923 2.272-3.863 1.558-9.253-1.743-12.388-.363-.345-.468-.415-.858-.728L16.124 2.022A10.708 10.708 0 009.32.012z"/></svg>';
    }

    private function createSlideIndicator(): string
    {
        $slideIndicator = new TagBuilder('div');
        $slideIndicator->addClasses('imt-slider-slide-indicator', $this->slideIndicator);

        $slideCircle = new TagBuilder('div');
        $slideCircle->addClasses('slide-circle');
        $slideCircle->forceClosingTag(true);
        $imageCircleString = $slideCircle->render();

        $imageCircles = '';
        for ($x = 1; $x <= $this->slides; $x++) {
            $imageCircles .= $imageCircleString;
        }

        $slidePosition = new TagBuilder('div');
        $slidePosition->addClasses('slide-position');
        $slidePosition->forceClosingTag(true);

        $slideIndicator->setContent($imageCircles . $slidePosition->render());

        return $slideIndicator->render();
    }

    private function createTimeoutIndicator(): string
    {
        $timeoutIndicator = new TagBuilder('div');
        $timeoutIndicator->forceClosingTag(true);
        $timeoutIndicator->addClasses('imt-slider-timeout-indicator', $this->timeoutIndicator);

        return $timeoutIndicator->render();
    }
}
