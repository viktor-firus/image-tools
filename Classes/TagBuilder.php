<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class TagBuilder extends \TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder
{
    /**
     * @var string[]
     */
    private $classes = [];

    /**
     * @var string[]
     */
    private $styles = [];

    public function addClasses(string ...$classes)
    {
        foreach ($classes as $class) {
            $this->classes = array_merge($this->classes, GeneralUtility::trimExplode(' ', $class, true));
        }
    }

    public function addStyle(string $propertyName, string $value)
    {
        $this->styles[$propertyName] = $value;
    }

    public function render()
    {
        $classes = implode(' ', $this->classes);
        $classes && $this->addAttribute('class', $classes);

        $styles = '';
        foreach ($this->styles as $propertyName => $value) {
            $styles .= $propertyName . ': ' . $value . ';';
        }
        $styles && $this->addAttribute('style', $styles);

        return parent::render();
    }
}
