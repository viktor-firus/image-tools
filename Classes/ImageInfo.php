<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools;

use ByteCube\ImageTools\ImageInfoInterface;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Extbase\Service\ImageService;

class ImageInfo implements ImageInfoInterface
{
    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * @var FileInterface
     */
    private $file;

    public function injectImageService(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function __construct(FileInterface $file)
    {
        $this->file = $file;
    }

    public function getHashIdentifier(): string
    {
        return (string)$this->file->getSha1();
    }

    public function getWidth(): int
    {
        return (int)$this->file->getProperty('width');
    }

    public function getHeight(): int
    {
        return (int)$this->file->getProperty('height');
    }

    public function getAlt(): string
    {
        return (string)$this->file->getProperty('alternative');
    }

    public function getTitle(): string
    {
        return (string)$this->file->getProperty('title');
    }

    public function getPath(): string
    {
        return $this->file->getForLocalProcessing(false);
    }

    public function getRelativeURL(): string
    {
        return $this->imageService->getImageUri($this->file, false);
    }

    public function getAbsoluteURL(): string
    {
        return $this->imageService->getImageUri($this->file, true);
    }
}
