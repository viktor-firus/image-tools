<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools;

use InvalidArgumentException;

class Configuration
{
    /**
     * @var array
     */
    private static $configuration;

    /**
     * @var int[]
     */
    private static $pixelDensities;

    /**
     * @var array
     */
    private static $sliders;

    public static function getSliderConfig(string $key): array
    {
        $config = self::getConfiguration();

        return $config['sliders'][$key]['config'];
    }

    public static function hasBreakpoint(string $key, int $breakpoint): bool
    {
        $config = self::getConfiguration();

        return isset($config['sliders'][$key]['config'][$breakpoint]);
    }

    public static function assertBreakpointExists(string $configKey, int $breakpoint)
    {
        if (!Configuration::hasBreakpoint($configKey, $breakpoint)) {
            throw new InvalidArgumentException('Breakpoint "' . $breakpoint . '" not found in slider "' . $configKey . '".');
        }
    }

    public static function getConfiguration(): array
    {
        if (self::$configuration) {
            return self::$configuration;
        }

        $sliders = self::$sliders;
        $slidersNew = [];
        foreach ($sliders as $key => $slider) {
            ksort($slider['config']);

            $slidersNew[$key]['name'] = $slider['name'];

            $previousBreakPoint = null;
            foreach ($slider['config'] as $breakpoint => $baseWidth) {
                $slidersNew[$key]['config'][$breakpoint]['from'] = $breakpoint ?: null;
                $slidersNew[$key]['config'][$breakpoint]['to'] = null;

                $slidersNew[$key]['config'][$breakpoint]['baseWidth'] = $baseWidth;

                if ($previousBreakPoint !== null) {
                    $slidersNew[$key]['config'][$previousBreakPoint]['to'] = $breakpoint - 1;
                }

                $previousBreakPoint = $breakpoint;
            }
        }

        self::$configuration = [
            'pixelDensities' => self::$pixelDensities,
            'sliders' => $slidersNew,
        ];

        return self::$configuration;
    }

    public static function setDefaults()
    {
        if (!self::$pixelDensities) {
            self::setPixelDensities(1, 2);
        }

        self::addSlider('default', 'Default');
        self::addSliderConfiguration('default', 0, 345);
        self::addSliderConfiguration('default', 576, 461);
        self::addSliderConfiguration('default', 768, 595);
        self::addSliderConfiguration('default', 992, 720);
        self::addSliderConfiguration('default', 1200, 840);
        self::addSliderConfiguration('default', 1400, 960);
        self::addSliderConfiguration('default', 1600, 1080);
        self::addSliderConfiguration('default', 1800, 1400);
    }

    public static function setPixelDensities(int ...$pixelDensities)
    {
        self::$pixelDensities = [];
        foreach ($pixelDensities as $pixelDensity) {
            self::addPixelDensity($pixelDensity);
        }
    }

    public static function addPixelDensity(int $pixelDensity)
    {
        self::$pixelDensities[] = $pixelDensity;
    }

    public static function addSlider(string $key, string $name)
    {
        self::$sliders[$key] = [
            'name' => $name,
            'config' => [],
        ];
    }

    /**
     * @param string $key        The key to identify the slider configuration
     * @param int    $breakpoint The breakpoint from which size the slider view is visible
     * @param int    $baseWidth  Width of the slider view to calculate optimal size of pictures for the given breakpoint
     */
    public static function addSliderConfiguration(string $key, int $breakpoint, int $baseWidth)
    {
        self::assertKeyExists($key);

        self::$sliders[$key]['config'][$breakpoint] = $baseWidth;
    }

    private static function assertKeyExists(string $key)
    {
        if (!isset(self::$sliders[$key])) {
            throw new InvalidArgumentException('Slider with key "' . $key . '" not exists.');
        }
    }
}
