<?php declare(strict_types = 1);
/**
 * This file is part of ViktorFirus/ImageTools.
 *
 * ViktorFirus/ImageTools is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * ViktorFirus/ImageTools is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ViktorFirus/ImageTools or see <http://www.gnu.org/licenses/>.
 */

namespace ViktorFirus\ImageTools;

use ByteCube\ImageTools\Crop\Crop;
use ByteCube\ImageTools\Crop\CropCollection;
use ByteCube\ImageTools\Dimension;
use ByteCube\ImageTools\Image\Image;
use ByteCube\ImageTools\Image\ImageInterface;
use ByteCube\ImageTools\ImageService\ImageServiceInterface;
use InvalidArgumentException;
use RuntimeException;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;

class ImageFactory implements SingletonInterface
{
    /**
     * @var ImageFactory
     */
    protected static $instance;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var FileRepository
     */
    protected $fileRepository;

    /**
     * @var ImageServiceInterface
     */
    protected $imageService;

    public function injectObjectManager(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function injectFileRepository(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    public function injectImageService(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function create($image): ImageInterface
    {
        if ($image instanceof ImageInterface) {
            return $image;
        }

        $image = $this->resolveInitialInput($image);

        if ($image instanceof FileReference) {
            return $this->createFromFileReference($image);
        }

        if ($image instanceof FileInterface) {
            return $this->createFromFile($image);
        }

        throw new InvalidArgumentException('Cannot resolve image.', 1535726505);
    }

    protected function createFromFileReference(FileReference $fileReference): ImageInterface
    {
        $crop = $this->createCropFromFileReference($fileReference);
        $file = $fileReference->getOriginalFile();
        $imageInfo = $this->objectManager->get(ImageInfo::class, $file);
        $storage = $this->objectManager->get(Storage::class, $file->getStorage());

        return new Image($imageInfo, $file, $storage, $this->imageService, null, $crop);
    }

    protected function createFromFile(FileInterface $file): Image
    {
        $imageInfo = $this->objectManager->get(ImageInfo::class, $file);
        $storage = $this->objectManager->get(Storage::class, $file->getStorage());

        return new Image($imageInfo, $file, $storage, $this->imageService);
    }

    protected function resolveInitialInput($image)
    {
        if (is_string($image)) {
            return $this->resolveString($image);
        }

        if ($image instanceof \TYPO3\CMS\Extbase\Domain\Model\FileReference) {
            return $image->getOriginalResource();
        }

        return $image;
    }

    protected function resolveString(string $image)
    {
        $id = $this->extractId($image, 'fileReference:') ?: $this->extractId($image, 'r:');
        if ($id) {
            return $this->findFileReferenceById($id);
        }

        $id = $this->extractId($image, 'file:') ?: $this->extractId($image, 'f:');
        if ($id) {
            return $this->findFileById($id);
        }

        return null;
    }

    protected function findFileReferenceById(int $id)
    {
        return $this->fileRepository->findFileReferenceByUid($id) ?: null;
    }

    protected function findFileById(int $id)
    {
        try {
            return $this->fileRepository->findByUid($id);
        } catch (RuntimeException $exception) {
            if ($exception->getCode() === 1314354065) {
                return null;
            }

            throw $exception;
        }
    }

    protected function extractId(string $image, string $prefix): ?int
    {
        $prefixLength = strlen($prefix);

        if (substr($image, 0, $prefixLength) !== $prefix) {
            return null;
        }

        return (int)substr($image, $prefixLength);
    }

    protected function createCropFromFileReference(FileReference $fileReference): CropCollection
    {
        $cropCollection = new CropCollection();

        $cropList = json_decode($fileReference->getProperty('crop'), true) ?: [];

        foreach ($cropList as $cropName => $cropData) {
            $cropArea = $cropData['cropArea'];
            $focusArea = @$cropData['focusArea'];

            $crop = (new Crop())
                ->withCropArea(new Dimension(
                    (float)$cropArea['x'],
                    (float)$cropArea['y'],
                    (float)$cropArea['width'],
                    (float)$cropArea['height']
                ));

            if (is_array($focusArea)) {
                $crop = $crop->withFocusArea(
                    new Dimension(
                        (float)$focusArea['x'],
                        (float)$focusArea['y'],
                        (float)$focusArea['width'],
                        (float)$focusArea['height']
                    )
                );
            }

            $cropCollection->add((string)$cropName, $crop);
        }

        return $cropCollection;
    }
}
