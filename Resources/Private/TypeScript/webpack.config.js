const path = require('path');

module.exports = (env) => {
    if (typeof env !== 'object' || env === null) {
        env = {};
    }

    const result = {
        entry: './index.ts',
        output: {
            path: path.resolve(__dirname, '../../Public/JavaScript'),
            filename: 'script.js',
        },
        resolve: {
            modules: ['node_modules', 'src'],
            extensions: ['.ts', '.js']
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: [
                                    [
                                        '@babel/preset-env',
                                        {
                                            'targets': '> 0.01%, not dead',
                                            'useBuiltIns': 'usage',
                                            'debug': true
                                        }
                                    ]
                                ],
                                plugins: ['@babel/plugin-transform-runtime']
                            }
                        },
                        {
                            loader: 'ts-loader',
                            options: {
                                configFile: 'tsconfig.json'
                            }
                        },
                        {
                            loader: 'tslint-loader',
                            options: {
                                configFile: 'tslint.json',
                                emitErrors: false,
                                failOnHint: false,
                                typeCheck: true,
                                fix: false
                            }
                        }
                    ]
                }
            ]
        },
        plugins: []
    };

    if (env.prod === true) {
        result.mode = 'production';
    } else {
        result.mode = 'development';
        result.devtool = 'inline-source-map';
    }

    return result;
};
