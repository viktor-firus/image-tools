import {AbstractImage} from "./abstract-image";
import {DummyImage} from "./dummy-image";
import {ImgImage} from "./img-image";
import {PictureImage} from "./picture-image";

enum Type {
    IMG,
    PICTURE,
}

export class ImageFactory {
    public static build(element: HTMLElement): AbstractImage {
        if (!element.hasAttribute("data-imt-ll")) {
            return new DummyImage();
        }

        const type = element.querySelector("picture") === null ? Type.IMG : Type.PICTURE;
        const animated = element.hasAttribute("data-imt-ll-animation");

        if (type === Type.PICTURE) {
            return new PictureImage(element, animated);
        }

        return new ImgImage(element, animated);
    }
}
