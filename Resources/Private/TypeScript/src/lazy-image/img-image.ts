import {AbstractImage} from "./abstract-image";

export class ImgImage extends AbstractImage {
    constructor(element: HTMLElement, animated: boolean) {
        super(element, animated);

        this.placeholderElement = this.container.querySelector("img");
        const pictureHtml = this.placeholderElement.outerHTML;

        this.placeholderElement.classList.add("placeholder");
        this.transitionPlaceholder = this.placeholderElement;

        const parseBox = document.createElement("div");
        parseBox.innerHTML = pictureHtml;
        this.pictureElement = parseBox.querySelector("img");
        this.pictureElement.classList.add("original");
        this.transitionPicture = this.pictureElement as HTMLImageElement;
    }

    protected insertPicture(ready: () => void) {
        const img = this.pictureElement as HTMLImageElement;

        if (img.dataset.srcset !== void 0) {
            img.setAttribute("srcset", img.dataset.srcset);
            img.removeAttribute("data-srcset");
        }

        img.setAttribute("src", img.dataset.src);
        img.removeAttribute("data-src");

        this.container.appendChild(this.pictureElement);

        this.onLoadPicture(img, () => ready());
    }
}
