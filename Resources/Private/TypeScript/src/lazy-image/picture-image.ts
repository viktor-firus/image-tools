import {AbstractImage} from "./abstract-image";

export class PictureImage extends AbstractImage {
    constructor(element: HTMLElement, animated: boolean) {
        super(element, animated);

        this.placeholderElement = this.container.querySelector("picture");
        const pictureHtml = this.placeholderElement.outerHTML;

        this.transitionPlaceholder = this.placeholderElement.querySelector("img");
        this.transitionPlaceholder.classList.add("placeholder");

        const parseBox = document.createElement("div");
        parseBox.innerHTML = pictureHtml;
        this.pictureElement = parseBox.querySelector("picture");
        this.transitionPicture = this.pictureElement.querySelector("img");
        this.transitionPicture.classList.add("original");
    }

    protected insertPicture(ready: () => void) {
        for (const source of Array.from(this.pictureElement.querySelectorAll("source")) as HTMLSourceElement[]) {
            source.setAttribute("srcset", source.dataset.srcset);
            source.removeAttribute("data-srcset");
        }

        this.transitionPicture.setAttribute("src", this.transitionPicture.dataset.src);
        this.transitionPicture.removeAttribute("data-src");

        this.container.appendChild(this.pictureElement);

        this.onLoadPicture(this.transitionPicture, () => ready());
    }
}
