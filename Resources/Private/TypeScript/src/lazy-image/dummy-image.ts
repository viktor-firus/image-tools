import {AbstractImage} from "./abstract-image";

export class DummyImage extends AbstractImage {
    // tslint:disable
    constructor() {
        super(null, null);
    }

    public showLoader() {
    }

    public showPicture() {
    }

    protected insertPicture(ready: () => void) {
    }

    // tslint:enable
}
