import {Utility} from "../utility";

enum LoadStep {
    Loader,
    Picture,
    Done,
    Hold,
}

export abstract class AbstractImage {
    protected container: HTMLElement;

    protected animated: boolean;

    protected pictureElement: HTMLElement;

    protected placeholderElement: HTMLElement;

    protected transitionPicture: HTMLImageElement;

    protected transitionPlaceholder: HTMLElement;

    protected loader: HTMLDivElement;

    protected nextLoadStep = LoadStep.Loader;

    constructor(container: HTMLElement, animated: boolean) {
        this.container = container;
        this.animated = animated;

        this.container.removeAttribute("data-imt-ll");
    }

    public showLoader() {
        if (this.nextLoadStep === LoadStep.Loader) {
            this.nextLoadStep = LoadStep.Hold;
            this.activateLoader();
            this.nextLoadStep = LoadStep.Picture;
        }
    }

    public showPicture(ready?: () => void) {
        this.showLoader();

        if (this.nextLoadStep === LoadStep.Picture) {
            this.nextLoadStep = LoadStep.Hold;
            this.insertPicture(() => {
                this.animated ? this.showAnimated() : this.showNotAnimated();
                this.nextLoadStep = LoadStep.Done;
                ready && ready();
            });
        }
    }

    public isDone(): boolean {
        return this.nextLoadStep === LoadStep.Done;
    }

    protected abstract insertPicture(ready: () => void);

    protected showAnimated() {
        this.transitionPicture.classList.add("step-1");
        this.transitionPlaceholder.classList.add("step-1");

        this.flushCss();

        this.transitionPicture.classList.add("step-2");
        this.transitionPlaceholder.classList.add("step-2");
        this.container.classList.add("show");

        this.flushCss();

        this.transitionPicture.classList.add("step-3");
        this.transitionPlaceholder.classList.add("step-3");

        this.flushCss();

        setTimeout(() => {
            this.loader.parentNode.removeChild(this.loader);
            this.placeholderElement.parentNode.removeChild(this.placeholderElement);

            this.container.classList.remove("show");
            this.container.removeAttribute("data-imt-ll-animation");

            this.transitionPicture.classList.remove("original");
            this.transitionPicture.classList.remove("step-1");
            this.transitionPicture.classList.remove("step-2");
            this.transitionPicture.classList.remove("step-3");

            this.clear();
        }, this.calculateMaxDuration());
    }

    protected showNotAnimated() {
        this.loader.parentNode.removeChild(this.loader);
        this.placeholderElement.parentNode.removeChild(this.placeholderElement);

        this.transitionPicture.classList.remove("original");

        this.clear();
    }

    protected clear() {
        this.pictureElement = void 0;
        this.placeholderElement = void 0;
        this.transitionPicture = void 0;
        this.transitionPlaceholder = void 0;
        this.loader = void 0;
    }

    protected calculateMaxDuration(): number {
        let transitionDurationString = window.getComputedStyle(this.transitionPicture).transitionDuration;
        transitionDurationString += transitionDurationString.length > 0 ? "," : "";
        transitionDurationString += window.getComputedStyle(this.transitionPlaceholder).transitionDuration;

        const transitionDuration = transitionDurationString.split(",");
        let maxDuration = 0;

        for (const duration of transitionDuration) {
            const durationMillisecond = parseFloat(duration.trim()) * 1000;
            if (durationMillisecond > maxDuration) {
                maxDuration = durationMillisecond;
            }
        }

        return maxDuration;
    }

    protected activateLoader() {
        const parseBox = document.createElement("div");
        parseBox.innerHTML = "<div class='loader'>" +
            "<div class='dot'></div><div class='dot'></div><div class='dot'></div>" +
            "</div>";

        this.loader = parseBox.querySelector(".loader");
        this.container.appendChild(this.loader);
    }

    protected flushCss() {
        Utility.flushCss(this.container);
    }

    protected onLoadPicture(image: HTMLImageElement, handler: () => void) {
        if (image.complete) {
            handler();
            return;
        }

        image.addEventListener("load", handler);
    }
}
