import {VisibilityObserver} from "../visibility-observer";
import {ImageFactory} from "./image-factory";

export class Scanner {
    public static start() {
        Array.from(document.querySelectorAll("[data-imt-ll]")).forEach((lazyLoadElement) => {
            const target = lazyLoadElement as HTMLElement;
            const image = ImageFactory.build(lazyLoadElement as HTMLElement);

            VisibilityObserver.getInstance().observe(target, () => image.showPicture());
        });
    }
}
