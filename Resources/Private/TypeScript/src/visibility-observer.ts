import {Subscription} from "./subscription";

export class VisibilityObserver {
    public static getInstance(): VisibilityObserver {
        if (!VisibilityObserver.instance) {
            VisibilityObserver.instance = new VisibilityObserver();
        }

        return VisibilityObserver.instance;
    }

    protected static instance;

    protected targets: Map<HTMLElement, boolean> = new Map();

    protected responders: Map<HTMLElement, () => void> = new Map();

    protected nodeEvents: Map<Node, { targets: Set<HTMLElement>, subscription: Subscription }> = new Map();

    protected windowEvents: Subscription[] = [];

    protected constructor() {
    }

    public observe(target: HTMLElement, responder: () => void) {
        if (target.hasAttribute("data-imt-observed")) {
            return;
        }
        target.setAttribute("data-imt-observed", "");

        this.targets.set(target, false);
        this.responders.set(target, responder);

        this.checkTarget(target);
        this.startEvents(target);
    }

    protected check() {
        for (const target of Array.from(this.targets.keys())) {
            if (this.checkTarget(target)) {
                this.targets.delete(target);
                this.responders.delete(target);
            }
        }

        this.stopEvents();
    }

    protected checkTarget(target: HTMLElement): boolean {
        if (!this.targets.has(target) || this.targets.has(target) && this.targets.get(target) === true) {
            return true;
        }

        const targetClientRect = target.getBoundingClientRect();
        const isVisible = targetClientRect.top <= window.innerHeight &&
            targetClientRect.left <= window.innerWidth &&
            targetClientRect.bottom >= 0 &&
            targetClientRect.right >= 0;

        if (isVisible === false) {
            return false;
        }

        this.responders.get(target)();
        this.targets.set(target, true);

        return true;
    }

    protected startEvents(element: HTMLElement) {
        if (this.windowEvents.length === 0) {
            this.windowEvents.push(Subscription.from(window, "resize", () => this.check()));
            this.windowEvents.push(Subscription.from(window, "orientationchange", () => this.check()));
        }

        for (let node = element.parentNode; node; node = node.parentNode) {
            if (!this.nodeEvents.has(node)) {
                this.nodeEvents.set(node, {
                    targets: new Set(),
                    subscription: Subscription.from(node, "scroll", () => this.check()),
                });
            }

            const nodeEvent = this.nodeEvents.get(node);
            nodeEvent.targets.add(element);
        }
    }

    protected stopEvents() {
        for (const entry of Array.from(this.nodeEvents[Symbol.iterator]())) {
            let node: Node;
            let event: { targets: Set<HTMLElement>, subscription: Subscription };
            [node, event] = entry;

            for (const target of Array.from(event.targets.values())) {
                if (!this.targets.has(target)) {
                    event.targets.delete(target);
                }
            }

            if (event.targets.size === 0) {
                event.subscription.unsubscribe();
                this.nodeEvents.delete(node);
            }
        }

        if (this.nodeEvents.size === 0) {
            for (const windowEvent of this.windowEvents) {
                windowEvent.unsubscribe();
            }

            this.windowEvents = [];
        }
    }
}
