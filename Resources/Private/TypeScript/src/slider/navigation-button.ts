export enum NavigationDirection {
    PREVIOUS,
    NEXT,
}

export class NavigationButton {
    public static init(container: HTMLElement, direction: NavigationDirection, click: () => void) {
        const button = container.querySelector(this.getSelector(direction));

        if (!button) {
            return;
        }

        button.addEventListener("click", () => click());
    }

    protected static getSelector(direction: NavigationDirection): string {
        if (direction === NavigationDirection.PREVIOUS) {
            return "div.imt-slider-left-button";
        }

        return "div.imt-slider-right-button";
    }
}
