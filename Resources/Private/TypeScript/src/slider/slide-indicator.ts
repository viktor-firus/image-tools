import {Utility} from "../utility";

export class SlideIndicator {
    protected position: HTMLDivElement;

    protected circles: HTMLDivElement[];

    constructor(protected container: HTMLElement, protected click: (position: number) => void) {
        this.position = container.querySelector("div.imt-slider-slide-indicator div.slide-position");
        if (!this.position) {
            return;
        }

        this.circles = Array.from(container.querySelectorAll("div.imt-slider-slide-indicator div.slide-circle"));

        this.updateCircles(1);

        for (let i = 0; i < this.circles.length; i++) {
            const circle = this.circles[i];

            circle.addEventListener("click", () => {
                const isActive = !circle.classList.contains("inactive");
                isActive && this.click(i + 1);
            });
        }
    }

    public movePosition(position: number, duration: number) {
        if (!this.position) {
            return;
        }

        const circleWidth = 15;
        const circleGap = 5;
        const innerGap = 3;

        const left = ((position - 1) * (circleWidth + circleGap)) + innerGap;

        this.position.style.transitionDuration = duration + "ms";
        Utility.flushCss(this.position);
        this.position.style.left = left + "px";

        this.updateCircles(position);
        Utility.flushCss(this.position);

        setTimeout(() => {
            this.position.style.transitionDuration = null;
        }, duration);
    }

    protected updateCircles(position: number) {
        for (let i = 0; i < this.circles.length; i++) {
            const circle = this.circles[i];

            if ((position - 1) === i) {
                circle.classList.add("inactive");
                continue;
            }

            circle.classList.remove("inactive");
        }
    }
}
