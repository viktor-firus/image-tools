import {Utility} from "../utility";

export class TimeoutIndicator {
    protected indicator: HTMLDivElement;

    protected currentState: "run" | "stop" | "indefinite" = "stop";

    constructor(protected container: HTMLElement) {
        this.indicator = container.querySelector("div.imt-slider-timeout-indicator");
    }

    public getCurrentState() {
        return this.currentState;
    }

    public reset() {
        if (this.currentState === "stop") {
            return;
        }

        this.indicator.classList.remove("imt-progress");
        this.indicator.classList.remove("imt-indefinite");
        this.indicator.style.transition = null;
        this.indicator.style.backgroundSize = null;
        Utility.flushCss(this.indicator);

        this.currentState = "stop";
    }

    public start(duration: number) {
        if (!this.indicator) {
            return;
        }

        this.reset();

        this.indicator.classList.add("imt-progress");
        this.indicator.style.transition = "background-size " + duration + "ms linear";
        Utility.flushCss(this.indicator);

        this.indicator.style.backgroundSize = "100% 100%";
        Utility.flushCss(this.indicator);

        this.currentState = "run";
    }

    public activateIndefinite() {
        if (!this.indicator || this.currentState === "indefinite") {
            return;
        }

        this.reset();

        this.currentState = "indefinite";

        this.indicator.classList.add("imt-indefinite");
        Utility.flushCss(this.indicator);
    }
}
