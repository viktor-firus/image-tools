import {AbstractImage} from "../lazy-image/abstract-image";
import {ImageFactory} from "../lazy-image/image-factory";
import {Utility} from "../utility";
import {NavigationButton, NavigationDirection} from "./navigation-button";
import {SlideDirection} from "./slide-direction";
import {SlideIndicator} from "./slide-indicator";
import {TimeoutIndicator} from "./timeout-indicator";

export class Slider {
    protected readonly slideIndicator: SlideIndicator;

    protected readonly timeoutIndicator: TimeoutIndicator;

    protected readonly durations: Map<number, number>;

    protected readonly slides: number;

    protected readonly stories: HTMLDivElement[];

    protected autoSlideTimeout: number;

    protected currentPosition = 1;

    protected currentInSlide = false;

    constructor(protected element: HTMLElement) {
        if (!element.hasAttribute("data-imt-slider")) {
            return;
        }

        element.removeAttribute("data-imt-slider");

        this.slides = parseInt(element.getAttribute("data-slides"), 10);
        this.stories = Array.from(element.querySelectorAll("div.imt-slider-story")) as HTMLDivElement[];

        NavigationButton.init(element, NavigationDirection.PREVIOUS, () => {
            this.slidePrevious();
        });
        NavigationButton.init(element, NavigationDirection.NEXT, () => {
            this.slideNext();
        });

        this.slideIndicator = new SlideIndicator(element, (position) => {
            this.slidePosition(position, SlideDirection.DIRECT);
        });

        this.durations = this.parseDurations();
        this.timeoutIndicator = new TimeoutIndicator(element);
        this.startAutoSlide();

        const images = this.scanImages();
        this.loadImages(images);
    }

    protected parseDurations(): Map<number, number> {
        const durations = this.element.getAttribute("data-durations").split(",");
        const result = new Map();
        for (let i = 0; i < durations.length; i++) {
            result.set(i + 1, parseInt(durations[i], 10));
        }

        return result;
    }

    protected scanImages(): AbstractImage[] {
        const images: AbstractImage[] = [];
        const imagesSelector = "div.imt-slider-image-story > div.imt-ll";
        const imageElements = Array.from(this.element.querySelectorAll(imagesSelector)) as HTMLElement[];

        for (const imageElement of imageElements) {
            imageElement.setAttribute("data-imt-ll", "");
            imageElement.setAttribute("data-imt-ll-animation", "overlay");

            const image = ImageFactory.build(imageElement);
            image.showLoader();
            images.push(image);
        }

        return images;
    }

    protected loadImages(images: AbstractImage[]) {
        const allImagesLoaded = images.filter((image) => !image.isDone()).length === 0;
        if (allImagesLoaded) {
            return;
        }

        for (const image of images) {
            if (image.isDone()) {
                continue;
            }

            image.showPicture(() => {
                this.loadImages(images);
            });

            return;
        }
    }

    protected slideNext() {
        this.slidePosition(this.getNextPosition(), SlideDirection.NEXT);
    }

    protected slidePrevious() {
        this.slidePosition(this.getPreviousPosition(), SlideDirection.PREVIOUS);
    }

    protected getNextPosition(): number {
        return this.currentPosition === this.slides
            ? 1
            : this.currentPosition + 1;
    }

    protected getPreviousPosition(): number {
        return this.currentPosition === 1
            ? this.slides
            : this.currentPosition - 1;
    }

    protected slidePosition(position: number, direction: SlideDirection) {
        if (this.currentInSlide) {
            return;
        }
        this.currentInSlide = true;

        this.stopAutoSlide();
        this.timeoutIndicator.reset();

        const transition = direction !== SlideDirection.DIRECT;
        let transitionClass: string = void 0;
        if (transition) {
            transitionClass = direction === SlideDirection.NEXT
                ? "imt-slide-" + this.currentPosition + "-" + position
                : "imt-slide-" + position + "-" + this.currentPosition;
        }

        transition && this.element.classList.add(transitionClass);
        Utility.flushCss(this.element);

        const duration = this.getSlideDuration();

        this.createSlideClasses(position).forEach((active, cssClass) => {
            active
                ? this.element.classList.add(cssClass)
                : this.element.classList.remove(cssClass);

            Utility.flushCss(this.element);
        });

        this.slideIndicator.movePosition(position, duration);

        this.currentPosition = position;

        setTimeout(() => {
            this.currentInSlide = false;
            transition && this.element.classList.remove(transitionClass);
            this.startAutoSlide();
        }, duration);
    }

    protected parseTransitionTime(time: string): number {
        let result = 0;

        const parts = time.split(",");
        for (const part of parts) {
            const partNumber = parseFloat(part.trim()) * 1000;
            if (partNumber > result) {
                result = partNumber;
            }
        }

        return result;
    }

    protected createSlideClasses(position: number): Map<string, boolean> {
        const result = new Map();

        for (let i = 1; i <= this.slides; i++) {
            result.set("imt-slide-" + i, position >= i);
        }

        return result;
    }

    protected getSlideDuration(): number {
        let duration = 0;

        for (const story of this.stories) {
            const storyDuration = this.parseTransitionTime(window.getComputedStyle(story).transitionDelay) +
                this.parseTransitionTime(window.getComputedStyle(story).transitionDuration);

            if (storyDuration > duration) {
                duration = storyDuration;
            }
        }

        return duration;
    }

    protected startAutoSlide() {
        this.stopAutoSlide();

        const duration = this.durations.get(this.currentPosition);

        if (this.timeoutIndicator.getCurrentState() !== "indefinite") {
            this.timeoutIndicator.start(duration);
        }

        this.autoSlideTimeout = setTimeout(() => {
            this.autoSlideTimeout = void 0;
            this.slideNext();
        }, duration);
    }

    protected stopAutoSlide() {
        if (!this.autoSlideTimeout) {
            return;
        }

        clearTimeout(this.autoSlideTimeout);
        this.autoSlideTimeout = void 0;
    }
}
