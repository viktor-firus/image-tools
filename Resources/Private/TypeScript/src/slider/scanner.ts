import {Slider} from "./slider";

export class Scanner {
    public static start() {
        Array.from(document.querySelectorAll("[data-imt-slider]")).forEach((sliderElement) => {
            const target = sliderElement as HTMLElement;
            const slider = new Slider(target);
        });
    }
}
