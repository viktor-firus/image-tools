export class Subscription {
    public static from(element: EventTarget, type: string, listener: EventListenerOrEventListenerObject): Subscription {
        return new Subscription(element, type, listener);
    }

    protected constructor(
        protected element: EventTarget,
        protected type: string,
        protected listener: EventListenerOrEventListenerObject,
    ) {
        element.addEventListener(type, listener);
    }

    public unsubscribe() {
        this.element.removeEventListener(this.type, this.listener);
    }
}
