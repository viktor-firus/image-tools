import {Scanner as LazyImageScanner} from "./src/lazy-image/scanner";
import {Scanner as SliderScanner} from "./src/slider/scanner";

(window as any).imageTools = {
    scanLazyLoad: () => LazyImageScanner.start(),
    scanSlider: () => SliderScanner.start(),
};

window.addEventListener("load", () => {
    LazyImageScanner.start();
    SliderScanner.start();
});
