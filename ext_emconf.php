<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'Image Tools',
    'description' => 'This extension is a toolkit to generate responsive images in fluid templates (with optional lazy loading and opportunity to define animations yourself for lazy loading).',
    'category' => 'services',
    'author' => 'Viktor Firus',
    'author_email' => 'viktor@neunzehn86.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '2.0.0-dev',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.10-9.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
