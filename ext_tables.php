<?php
!defined('TYPO3_MODE') && die('Access denied.');

call_user_func(
    function () {
        $demoActive = (bool)@$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['image_tools']['demoModule'];

        if (TYPO3_MODE === 'BE' && $demoActive) {
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'ViktorFirus.ImageTools',
                'system',
                'image_tools',
                '',
                [
                    'Demo' => 'cropFocusAspectRatio,crop,img,lazyLoad,urlList,slider',
                ],
                [
                    'access' => 'admin',
                    'icon' => 'EXT:image_tools/mod_icon.png',
                    'labels' => 'LLL:EXT:image_tools/Resources/Private/Language/locallang_mod.xlf',
                ]
            );
        }

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
            'ViktorFirus.ImageTools',
            'site',
            'image_tools',
            'bottom',
            [
                'Slider' => 'index',
            ],
            [
                'access' => 'group',
                'icon' => 'EXT:image_tools/mod_icon.png',
                'labels' => 'LLL:EXT:image_tools/Resources/Private/Language/locallang_slider_mod.xlf',
            ]
        );
    }
);